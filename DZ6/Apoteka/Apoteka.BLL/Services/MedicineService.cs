﻿using Apoteka.BLL.Models;
using Apoteka.BLL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.BLL.Services
{
    public class MedicineService
    {
        private IRepositoryContext context;

        public MedicineService(IRepositoryContext context)
        {
            this.context = context;
        }

        public Medicine Get(int id)
        {
            return context.Medicines.Get(id);
        }

        public List<Medicine> GetAll()
        {
            return context.Medicines.GetAll();
        }
    }
}
