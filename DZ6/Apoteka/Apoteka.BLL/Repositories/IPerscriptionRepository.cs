﻿using Apoteka.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.BLL.Repositories
{
    public interface IPerscriptionRepository : IBaseRepository<Perscription>
    {
        List<PerscriptionMedicine> GetMedicines(int id);

        List<PerscriptionMedicine> GetMedicines(Perscription eo);

        List<PerscriptionMedicine> UpdateMedicines(int id, List<PerscriptionMedicine> eom);

        List<PerscriptionMedicine> UpdateMedicines(Perscription eo, List<PerscriptionMedicine> eom);
    }
}
