﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.BLL.Repositories
{
    public interface IRepositoryContext : IDisposable
    {
        IEmployeeRepository Employees { get; }

        IExternalOrderRepository ExternalOrders { get; }

        IMedicineRepository Medicines { get; }

        IOfficeRepository Offices { get; }

        IPerscriptionRepository Perscriptions { get; }

        ISupplierRepository Suppliers { get; }
    }
}
