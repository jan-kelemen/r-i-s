﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.BLL.Models
{
    public class LocalSupply : ModelBase
    {
        private Medicine medicine;

        private int supply;

        private int? safetySupply;

        public LocalSupply(int id, Medicine medicine, int supply, int? safetySupply = null) : base(id)
        {
            Medicine = medicine;
            Supply = supply;
            SafetySupply = safetySupply;
        }

        public Medicine Medicine
        {
            get { return medicine; }
            set
            {
                medicine = value ?? throw new ApplicationException("Invalid local supply medicine value");
            }
        }

        public int Supply
        {
            get { return supply; }
            set
            {
                if(value < 0)
                {
                    throw new ApplicationException("Invalid local supply value");
                }
                supply = value;
            }
        }

        public int? SafetySupply
        {
            get { return safetySupply; }
            set
            {
                if(value.HasValue && value.Value < 0)
                {
                    throw new ApplicationException("Invalid local safety supply value");
                }
                safetySupply = value;
            }
        }
    }
}
