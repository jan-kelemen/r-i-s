﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.BLL.Models
{
    public class Supplier : ModelBase
    {
        public const int MAX_NAME_LENGTH = 80;

        public const int MAX_ADDRESS_LENGTH = 200;

        private string name;

        private string address;

        public Supplier(int id, string name, string address) : base(id)
        {
            Name = name;
            Address = address;
        }

        public string Name
        {
            get { return name; }
            set
            {
                checkTextField(value, MAX_NAME_LENGTH, "Invalid supplier name value");
                name = value;
            }
        }
        public string Address
        {
            get { return address; }
            set
            {
                checkTextField(value, MAX_ADDRESS_LENGTH, "Invalid supplier address value");
                address = value;
            }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
