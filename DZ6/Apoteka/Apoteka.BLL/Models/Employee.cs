﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.BLL.Models
{
    public class Employee : ModelBase
    {
        public const int MAX_FIRST_NAME_LENGTH = 80;

        public const int MAX_LAST_NAME_LENGTH = 80;

        public const int MAX_EMAIL_LENGTH = 254;

        public const int PASSWORD_HASH_LENGTH = 32;

        private string firstName;

        private string lastName;

        private string email;

        private string passwordHash;

        private Office office;

        private EmployeeRole role;

        public Employee(int id, string firstName, string lastName, 
            string email, string passwordHash, Office office = null, EmployeeRole role = null) 
            : base(id)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            PasswordHash = passwordHash;
            Office = office;
            Role = role;
        }

        public string FirstName
        {
            get { return firstName; }
            set
            {
                checkTextField(value, MAX_FIRST_NAME_LENGTH, "Invalid employee first name value");
                firstName = value;
            }
        }

        public string LastName
        {
            get { return lastName; }
            set
            {
                checkTextField(value, MAX_LAST_NAME_LENGTH, "Invalid employee last name value");
                lastName = value;
            }
        }

        public string Email
        {
            get { return email; }
            set
            {
                checkTextField(value, MAX_EMAIL_LENGTH, "Invalid employee email value");
                email = value;
            }
        }

        public string PasswordHash
        {
            get { return passwordHash; }
            set
            {
                checkTextFieldExact(value, PASSWORD_HASH_LENGTH, "Invalid employee password hash");
                passwordHash = value;
            }
        }

        public Office Office
        {
            get { return office; }
            set { office = value; }
        }

        public EmployeeRole Role
        {
            get { return role; }
            set { role = value; }
        }

        public override string ToString()
        {
            return FirstName + " " + LastName;
        }
    }
}
