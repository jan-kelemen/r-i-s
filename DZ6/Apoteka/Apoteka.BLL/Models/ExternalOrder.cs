﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.BLL.Models
{
    public class ExternalOrder : ModelBase
    {
        private DateTime issuedOn;

        private Supplier supplier;

        private Employee employee;

        public ExternalOrder(int id, DateTime issuedOn, Supplier supplier, Employee employee = null) : base(id)
        {
            IssuedOn = issuedOn;
            Supplier = supplier;
            Employee = employee;
        }

        public DateTime IssuedOn
        {
            get { return issuedOn; }
            set { issuedOn = value; }
        }

        public Supplier Supplier
        {
            get { return supplier; }
            set
            {
                supplier = value ?? throw new ApplicationException("Invalid external order supplier");
            }
        }

        public Employee Employee
        {
            get { return employee; }
            set { employee = value; }
        }
    }
}
