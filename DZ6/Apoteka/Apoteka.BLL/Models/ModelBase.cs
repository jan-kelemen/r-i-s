﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.BLL.Models
{
    public class ModelBase
    {
        private int id;

        public ModelBase(int id = 0)
        {
            Id = id;
        }

        public int Id
        {
            get
            {
                return id;
            }
            private set
            {
                id = value;
            }
        }

        protected void checkTextField(string value, int length, string excMessage)
        {
            if (value == null || value.Length > length)
            {
                throw new ApplicationException(excMessage);
            }
        }

        protected void checkTextFieldExact(string value, int length, string excMessage)
        {
            if(value == null || value.Length != length)
            {
                throw new ApplicationException(excMessage);
            }
        }

        public override bool Equals(object obj)
        {
            if(obj == null || !(obj is ModelBase))
            {
                return false;
            }
            var o = obj as ModelBase;

            return Id == o.Id;
        }

        public override int GetHashCode()
        {
            return id.GetHashCode();
        }
    }
}
