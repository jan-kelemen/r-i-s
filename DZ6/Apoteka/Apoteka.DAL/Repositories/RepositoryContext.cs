﻿using Apoteka.BLL.Repositories;
using Apoteka.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.DAL.Repositories
{
    public class RepositoryContext : IRepositoryContext
    {
        private RISEntities db;

        private IEmployeeRepository employees;

        private IExternalOrderRepository externalOrders;

        private IMedicineRepository medicines;

        private IOfficeRepository offices;

        private IPerscriptionRepository perscriptions;

        private ISupplierRepository suppliers;

        public RepositoryContext()
        {
            db = new RISEntities();
            employees = new EmployeeRepository(db);
            externalOrders = new ExternalOrderRepository(db);
            medicines = new MedicineRepository(db);
            offices = new OfficeRepository(db);
            perscriptions = new PerscriptionRepository(db);
            suppliers = new SupplierRepository(db);
        }

        public IEmployeeRepository Employees => employees;

        public IExternalOrderRepository ExternalOrders => externalOrders;

        public IMedicineRepository Medicines => medicines;

        public IOfficeRepository Offices => offices;

        public IPerscriptionRepository Perscriptions => perscriptions;

        public ISupplierRepository Suppliers => suppliers;

        public void Dispose()
        {
            db.Dispose();
        }
    }
}
