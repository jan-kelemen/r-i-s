﻿using Apoteka.BLL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Apoteka.BLL.Models;
using Apoteka.DAL.Converters;

namespace Apoteka.DAL.Repositories
{
    public class SupplierRepository : BaseRepository<Model.Supplier, Supplier>, ISupplierRepository
    {
        public SupplierRepository(Model.RISEntities db) : base(db)
        {
        }

        public List<Medicine> GetMedicines(int id)
        {
            var medicines = db.Suppliers.Find(id).Medicines;

            var rv = new List<Medicine>();
            foreach(var m in medicines)
            {
                rv.Add(BLLConverter.ConvertMedicine(m));
            }

            return rv;
        }

        public List<Medicine> GetMedicines(Supplier eo)
        {
            return GetMedicines(eo.Id);
        }

        public List<Medicine> UpdateMedicines(int id, List<Medicine> eom)
        {
            var supplier = db.Suppliers.Find(id);
            supplier.Medicines.Clear();
            db.SaveChanges();

            foreach(var m in eom)
            {
                supplier.Medicines.Add(db.Medicines.Find(m.Id));
            }

            var rv = new List<Medicine>();
            foreach(var m in supplier.Medicines)
            {
                rv.Add(BLLConverter.ConvertMedicine(m));
            }

            return rv;
        }

        public List<Medicine> UpdateMedicines(Supplier eo, List<Medicine> eom)
        {
            return UpdateMedicines(eo.Id, eom);
        }

        protected override Supplier convertToBLL(Model.Supplier e)
        {
            return BLLConverter.ConvertSupplier(e);
        }

        protected override Model.Supplier convertToDAL(Supplier e)
        {
            return DALConverter.ConvertSupplier(e);
        }

        protected override void updateProperties(Model.Supplier dal, Supplier bll)
        {
            dal.Name = bll.Name;
            dal.Address = bll.Address;
        }
    }
}
