﻿using Apoteka.BLL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Apoteka.BLL.Models;
using Apoteka.DAL.Converters;

namespace Apoteka.DAL.Repositories
{
    public class EmployeeRepository : BaseRepository<Model.Employee, Employee>, IEmployeeRepository
    {
        private class EmployeeRoleRepository : BaseRepository<Model.EmployeeRole, EmployeeRole>
        {
            public EmployeeRoleRepository(Model.RISEntities db) : base(db)
            {
            }

            protected override EmployeeRole convertToBLL(Model.EmployeeRole e)
            {
                return BLLConverter.ConvertEmployeeRole(e);
            }

            protected override Model.EmployeeRole convertToDAL(EmployeeRole e)
            {
                return DALConverter.ConvertEmployeeRole(e);
            }

            protected override void updateProperties(Model.EmployeeRole dal, EmployeeRole bll)
            {
                dal.Name = bll.Name;
            }
        }

        private EmployeeRoleRepository roleRepository;

        public EmployeeRepository(Model.RISEntities db) : base(db)
        {
            roleRepository = new EmployeeRoleRepository(db);
        }

        public EmployeeRole CreateRole(EmployeeRole er)
        {
            return roleRepository.Create(er);
        }

        public bool DeleteRole(int id)
        {
            return roleRepository.Delete(id);
        }

        public bool DeleteRole(EmployeeRole er)
        {
            return roleRepository.Delete(er);
        }

        public List<EmployeeRole> GetAllRoles()
        {
            return roleRepository.GetAll();
        }

        public EmployeeRole GetRole(int id)
        {
            return roleRepository.Get(id);
        }

        public EmployeeRole UpdateRole(EmployeeRole er)
        {
            return roleRepository.Update(er);
        }

        protected override Model.Employee convertToDAL(Employee e)
        {
            return DALConverter.ConvertEmployee(e);
        }

        protected override Employee convertToBLL(Model.Employee e)
        {
            return BLLConverter.ConvertEmployee(e);
        }

        protected override void updateProperties(Model.Employee dal, Employee bll)
        {
            throw new NotImplementedException();
        }
    }
}
