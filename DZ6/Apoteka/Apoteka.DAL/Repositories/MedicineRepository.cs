﻿using Apoteka.BLL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Apoteka.BLL.Models;
using Apoteka.DAL.Converters;

namespace Apoteka.DAL.Repositories
{
    public class MedicineRepository : BaseRepository<Model.Medicine, Medicine>, IMedicineRepository
    {
        public MedicineRepository(Model.RISEntities db) : base(db)
        {
        }

        protected override Medicine convertToBLL(Model.Medicine e)
        {
            return BLLConverter.ConvertMedicine(e);
        }

        protected override Model.Medicine convertToDAL(Medicine e)
        {
            return DALConverter.ConvertMedicine(e);
        }

        protected override void updateProperties(Model.Medicine dal, Medicine bll)
        {
            dal.Id = bll.Id;
            dal.Name = bll.Name;
            dal.Description = bll.Description;
        }
    }
}
