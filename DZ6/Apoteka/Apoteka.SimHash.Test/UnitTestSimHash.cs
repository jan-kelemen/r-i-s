﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Apoteka.SimHash.Test
{
    [TestClass]
    public class UnitTestSimHash
    {
        [TestMethod]
        public void TestSimHash()
        {
            var res = SimHash.Calculate("fakultet elektrotehnike i racunarstva");
            Assert.AreEqual("f27c6b49c8fcec47ebeef2de783eaf57", SimHash.ConvertToHex(res));
        }

        [TestMethod]
        public void TestToHex()
        {
            Assert.AreEqual("8302", SimHash.ConvertToHex(new byte[] { 0x83, 0x02 }));
        }

        [TestMethod]
        public void TestHammingDistance()
        {
            Assert.AreEqual(8, SimHash.HammingDistance(new byte[] { 0xff }, new byte[] { 0x00 }));
            Assert.AreEqual(3, SimHash.HammingDistance(new byte[] { 0x03, 0x83 }, new byte[] { 0x83, 0x02 }));
        }
    }
}
