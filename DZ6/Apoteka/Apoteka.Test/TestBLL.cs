﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Apoteka.BLL.Models;
using Apoteka.DAL.Repositories;
using Apoteka.BLL.Services;

namespace Apoteka.Test
{
    [TestClass]
    public class TestBLL
    {
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void TestEmployeeWithWrongHash()
        {
            var emp = new Employee(0, "Milan", "Bandic", "milan@apoteka.com", "abc");

            Assert.AreEqual("Milan", emp.FirstName);
            Assert.AreEqual("Bandic", emp.LastName);
            Assert.AreEqual("milan@apoteka.com", emp.Email);
            Assert.AreEqual("01234567890123456789012345678901", emp.PasswordHash);
        }

        [TestMethod]
        public void TestEmployeeWithCorrectHash()
        {
            var emp = new Employee(0, "Milan", "Bandic", "milan@apoteka.com", "01234567890123456789012345678901");

            Assert.AreEqual("Milan", emp.FirstName);
            Assert.AreEqual("Bandic", emp.LastName);
            Assert.AreEqual("milan@apoteka.com", emp.Email);
            Assert.AreEqual("01234567890123456789012345678901", emp.PasswordHash);
        }

        [TestMethod]
        public void TestEmployeeService()
        {
            var cntx = new RepositoryContext();
            var service = new EmployeeService(cntx);
            var employee = service.Get(1);

            Assert.AreEqual(1, employee.Id);
            Assert.AreEqual("Halid", employee.FirstName);
            Assert.AreEqual("Bešlic", employee.LastName);

            Assert.AreEqual("halid@apoteka.com", employee.Email);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void TestEmployeeWithTooLongName()
        {
            var cntx = new RepositoryContext();
            var service = new EmployeeService(cntx);
            var employee = service.Get(1);

            Assert.AreEqual(1, employee.Id);
            Assert.AreEqual("Halid", employee.FirstName);
            Assert.AreEqual("Bešlic", employee.LastName);

            Assert.AreEqual("halid@apoteka.com", employee.Email);

            employee.FirstName = "01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789";

            Assert.AreEqual("01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789", employee.FirstName);
        }
    }
}
