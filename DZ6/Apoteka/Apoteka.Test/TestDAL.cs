﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Apoteka.DAL.Repositories;

namespace Apoteka.Test
{
    [TestClass]
    public class TestDAL
    {
        [TestMethod]
        public void TestEmployeeRepository()
        {
            var repo = new EmployeeRepository(new DAL.Model.RISEntities());

            var employee = repo.Get(1);

            Assert.AreEqual(1, employee.Id);
            Assert.AreEqual("Halid", employee.FirstName);
            Assert.AreEqual("Bešlic", employee.LastName);

            Assert.AreEqual("halid@apoteka.com", employee.Email);
        }
    }
}
