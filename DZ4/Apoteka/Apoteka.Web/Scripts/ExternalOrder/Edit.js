﻿$(function () {
    $("#container").on('click', '#add-medicine-btn', function () {
        var row =
            '<tr class="success">\n' +
                '<td class="col-xs-6">\n' +
                    '<input name="MedicineEditors.Index" type="hidden" value="' + medicineCounter + '">\n' +
                    '<input name="MedicineEditors[' + medicineCounter + '].Id" type="hidden" value="-1">\n' +
                    '<input name="MedicineEditors[' + medicineCounter + '].State" type="hidden" value="n">\n' +
                    '<select class="form-control" id="medicine-selector[' + medicineCounter + ']" name="MedicineEditors[' + medicineCounter + '].SelectedMedicineId">' + medicines + '</select>\n' +
                '</td>\n' +
                '<td class="col-xs-5">\n' +
                    '<input class="form-control text-box single-line" name="MedicineEditors[' + medicineCounter + '].Amount" type="number" value="0">\n' +
                '</td>\n' +
                    '<td class="col-xs-1">\n' +
                    '<button type="button" class="btn btn-default button" id="remove-medicine-btn">Remove</button>\n' +
                '</td>\n' +
            '</tr>';

        $('#medicine-table tr:last').after(row);
        $("#medicine-selector\\[" + medicineCounter + "\\]").val(-1);

        medicineCounter++;
    });

    $("#container").on('click', '#cancel-medicine-btn', function () {
        var btn = $(this);
        btn.html('Remove');
        btn.prop('id', 'delete-medicine-btn');
        var tr = btn.closest('tr');
        tr.toggleClass('danger');

        var medicineFilter = "select[id^='medicine-selector']";
        var medicineValue = tr.find(componentFilter).val();

        var html = tr.html();
        var regex = /("|')d\1/gi;
        tr.html(html.replace(regex, "'e'"));

        tr.find(medicineFilter).val(medicineValue);
    });

    $("#container").on('click', '#delete-medicine-btn', function () {
        var btn = $(this);
        btn.html('Cancel');
        btn.prop('id', 'cancel-medicine-btn');

        var tr = btn.closest('tr');
        tr.toggleClass('danger');

        var medicineFilter = "select[id^='medicine-selector']";
        var medicineValue = tr.find(medicineFilter).val();

        var html = tr.html();
        var regex = /("|')e\1/gi;
        tr.html(html.replace(regex, "'d'"));

        tr.find(medicineFilter).val(medicineValue);
    });

    $("#container").on('click', '#remove-medicine-btn', function () {
        $(this).closest("tr").remove();
    });
});