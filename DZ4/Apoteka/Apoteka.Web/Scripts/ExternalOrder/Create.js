﻿$(function () {
    $("#container").on('click', '#add-medicine-btn', function () {
        var row =
            '<tr>\n' +
                '<td class="col-xs-6">\n' +
                    '<input name="MedicineEditors.Index" type="hidden" value="' + medicineCounter + '">\n' +
                    '<select class="form-control" id="medicine-selector[' + medicineCounter + ']" name="MedicineEditors[' + medicineCounter + '].SelectedMedicineId">' + medicines + '</select>\n' +
                '</td>\n' +
                '<td class="col-xs-5">\n' +
                    '<input class="form-control text-box single-line" name="MedicineEditors[' + medicineCounter + '].Amount" type="number" value="0">\n' +
                '</td>\n' +
                '<td class="col-xs-1">\n' +
                    '<button type="button" class="btn btn-default button" id="remove-btn">Remove</button>\n' +
                '</td>\n' +
            '</tr>';

        $('#medicine-table tr:last').after(row);
        $("#medicine-selector\\[" + medicineCounter + "\\]").val(-1);

        medicineCounter++;
    });

    $("#container").on('click', '#remove-btn', function () {
        if (confirm("Are you sure you want to remove the medicine?")) {
            $(this).closest("tr").remove();
        }
    });
});