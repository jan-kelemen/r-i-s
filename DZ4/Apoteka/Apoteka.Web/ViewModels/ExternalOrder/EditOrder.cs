﻿using Apoteka.BLL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Apoteka.Web.ViewModels.ExternalOrder
{
    public class EditOrder
    {
        public class MedicineEditor
        {
            public int Id { get; set; } = -1;

            public int SelectedMedicineId { get; set; } = -1;

            public int Amount { get; set; }

            public char State { get; set; }
        }

        public int Current { get; set; }

        public int Total { get; set; }

        public int Id { get; set; } = -1;

        [Display(Name = "Issued on")]
        [Required(ErrorMessage = "Issue time is required")]
        public DateTime IssuedOn { get; set; } = DateTime.Now;

        [Display(Name = "Employee")]
        [Required(ErrorMessage = "Employee is required")]
        public int SelectedEmployeeId { get; set; } = 0;

        public SelectList Employees { get; set; }

        [Display(Name = "Supplier")]
        [Required(ErrorMessage = "Supplier is required")]
        public int SelectedSupplierId { get; set; } = 0;

        public SelectList Suppliers { get; set; }

        public List<MedicineEditor> MedicineEditors { get; set; } = new List<MedicineEditor>();

        public List<Medicine> Medicines { get; set; }
    }
}