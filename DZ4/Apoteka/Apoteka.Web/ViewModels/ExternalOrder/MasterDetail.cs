﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Apoteka.Web.ViewModels.ExternalOrder
{
    public class MasterDetail
    {
        public class Medicine
        {
            public string MedicineName { get; set; }
            
            public int Amount { get; set; }
        }

        public int Current { get; set; }

        public int Total { get; set; }

        [Display(Name = "Order number")]
        public int Id { get; set; }

        [Display(Name = "Issued on")]
        public DateTime IssuedOn { get; set; }

        [Display(Name = "Employee")]
        public string EmployeeName { get; set; }

        [Display(Name = "Supplier")]
        public string SupplierName { get; set; }

        public List<Medicine> Medicines { get; set; }
    }
}