﻿using Apoteka.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.DAL.Converters
{
    public static class DALConverter
    {
        public static Model.Employee ConvertEmployee(Employee e)
        {
            return new Model.Employee
            {
                Id = e.Id,
                FirstName = e.FirstName,
                LastName = e.LastName,
                Email = e.Email,
                PasswordHash = e.PasswordHash,
                OfficeId = e.Office?.Id,
                RoleId = e.Role?.Id,
            };
        }

        public static Model.EmployeeRole ConvertEmployeeRole(EmployeeRole e)
        {
            return new Model.EmployeeRole
            {
                Id = e.Id,
                Name = e.Name,
            };
        }

        public static Model.ExternalOrder ConvertExternalOrder(ExternalOrder e)
        {
            return new Model.ExternalOrder
            {
                Id = e.Id,
                IssuedOn = e.IssuedOn,
                SupplierId = e.Supplier.Id,
                EmployeeId = e.Employee?.Id,
            };
        }

        public static Model.ExternalOrderMedicine ConvertExternalOrderMedicine(ExternalOrderMedicine e, int externalOrderId)
        {
            return new Model.ExternalOrderMedicine
            {
                Id = e.Id,
                ExternalOrderId = externalOrderId,
                MedicineId = e.Medicine.Id,
                Amount = e.Amount,
            };
        }

        public static Model.LocalSupply ConvertLocalSupply(LocalSupply e, int officeId)
        {
            return new Model.LocalSupply
            {
                Id = e.Id,
                MedicineId = e.Medicine.Id,
                OfficeId = officeId,
                Supply = e.Supply,
                SafetySupply = e.SafetySupply,
            };
        }

        public static Model.Medicine ConvertMedicine(Medicine e)
        {
            return new Model.Medicine
            {
                Id = e.Id,
                Name = e.Name,
                Description = e.Description,
            };
        }

        public static Model.Office ConvertOffice(Office e)
        {
            return new Model.Office
            {
                Id = e.Id,
                Name = e.Name,
                Address = e.Address,
            };
        }

        public static Model.Perscription ConvertPerscription(Perscription e)
        {
            return new Model.Perscription
            {
                Id = e.Id,
                CardNumber = e.CardNumber,
                IssuedOn = e.IssuedOn,
            };
        }

        public static Model.PerscriptionMedicine ConvertPerscriptionMedicine(PerscriptionMedicine e, int perscriptionId)
        {
            return new Model.PerscriptionMedicine
            {
                Id = e.Id,
                PerscriptionId = perscriptionId,
                MedicineId = e.Medicine.Id,
                Amount = e.Amount,
            };
        }

        public static Model.Supplier ConvertSupplier(Supplier e)
        {
            return new Model.Supplier
            {
                Id = e.Id,
                Name = e.Name,
                Address = e.Address,
            };
        }
    }
}
