﻿using Apoteka.BLL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.DAL.Repositories
{
    public abstract class BaseRepository<DALT, BLLT> : IBaseRepository<BLLT> where DALT : class
    {
        protected Model.RISEntities db;

        public BaseRepository(Model.RISEntities db)
        {
            this.db = db;
        }

        public BLLT Create(BLLT e)
        {
            var ent = convertToDAL(e);
            db.Set<DALT>().Add(ent);
            db.SaveChanges();

            return convertToBLL(db.Set<DALT>().Find(getId(ent)));
        }

        public BLLT Get(int id)
        {
            return convertToBLL(db.Set<DALT>().Find(id));
        }

        public List<BLLT> GetAll()
        {
            var rv = new List<BLLT>();
            foreach (var e in db.Set<DALT>())
            {
                rv.Add(convertToBLL(e));
            }
            return rv;
        }

        public BLLT Update(BLLT e)
        {
            var ent = db.Set<DALT>().Find(getId(e));
            updateProperties(ent, e);
            db.SaveChanges();
            return convertToBLL(ent);
        }
        
        public bool Delete(int id)
        {
            var ent = db.Set<DALT>().Find(id);
            db.Entry(ent).State = System.Data.Entity.EntityState.Deleted;

            var rv = db.SaveChanges() == 1;

            return rv;
        }

        public bool Delete(BLLT e)
        {
            if (e == null)
            {
                throw new ApplicationException("But why");
            }
            return Delete(getId(e));
        }

        protected abstract DALT convertToDAL(BLLT e);

        protected abstract BLLT convertToBLL(DALT e);

        protected abstract void updateProperties(DALT dal, BLLT bll);

        private static int getId(object source)
        {
            var type = source.GetType();
            var property = type.GetProperty("Id");
            return (int)property.GetValue(source);
        }
    }
}
