﻿using Apoteka.BLL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Apoteka.BLL.Models;
using Apoteka.DAL.Converters;

namespace Apoteka.DAL.Repositories
{
    public class ExternalOrderRepository : BaseRepository<Model.ExternalOrder, ExternalOrder>, IExternalOrderRepository
    {
        public ExternalOrderRepository(Model.RISEntities db) : base(db)
        {
        }

        public ExternalOrder Create(ExternalOrder eo, List<ExternalOrderMedicine> eom)
        {
            var order = DALConverter.ConvertExternalOrder(eo);
            db.ExternalOrders.Add(order);
            db.SaveChanges();
            foreach(var m in eom)
            {
                db.ExternalOrderMedicines.Add(DALConverter.ConvertExternalOrderMedicine(m, order.Id));
            }
            db.SaveChanges();

            return BLLConverter.ConvertExternalOrder(order);
        }

        public List<ExternalOrderMedicine> GetMedicines(int id)
        {
            var medicines = db.ExternalOrderMedicines.Where(m => m.ExternalOrderId == id);
            var rv = new List<ExternalOrderMedicine>();
            foreach(var m in medicines)
            {
                rv.Add(BLLConverter.ConvertExternalOrderMedicine(m));
            }
            return rv;
        }

        public List<ExternalOrderMedicine> GetMedicines(ExternalOrder eo)
        {
            return GetMedicines(eo.Id);
        }

        public List<ExternalOrderMedicine> UpdateMedicines(int id, List<ExternalOrderMedicine> eom)
        {
            var order = db.ExternalOrders.Find(id);
            db.ExternalOrderMedicines.RemoveRange(order.ExternalOrderMedicines);
            db.SaveChanges();

            var dalList = new List<Model.ExternalOrderMedicine>();
            foreach(var m in eom)
            {
                var dal = new Model.ExternalOrderMedicine
                {
                    ExternalOrderId = id,
                    MedicineId = m.Medicine.Id,
                    Amount = m.Amount,
                };
                db.ExternalOrderMedicines.Add(dal);
                dalList.Add(dal);
            }
            db.SaveChanges();

            var rv = new List<ExternalOrderMedicine>();
            foreach(var dal in dalList)
            {
                rv.Add(BLLConverter.ConvertExternalOrderMedicine(dal));
            }

            return rv;
        }

        public List<ExternalOrderMedicine> UpdateMedicines(ExternalOrder eo, List<ExternalOrderMedicine> eom)
        {
            return UpdateMedicines(eo.Id, eom);
        }

        protected override ExternalOrder convertToBLL(Model.ExternalOrder e)
        {
            return BLLConverter.ConvertExternalOrder(e);
        }

        protected override Model.ExternalOrder convertToDAL(ExternalOrder e)
        {
            return DALConverter.ConvertExternalOrder(e);
        }

        protected override void updateProperties(Model.ExternalOrder dal, ExternalOrder bll)
        {
            dal.IssuedOn = bll.IssuedOn;
            dal.SupplierId = bll.Supplier.Id;
            dal.EmployeeId = bll.Employee?.Id;
        }
    }
}
