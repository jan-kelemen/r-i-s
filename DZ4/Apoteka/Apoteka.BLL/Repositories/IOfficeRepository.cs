﻿using Apoteka.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.BLL.Repositories
{
    public interface IOfficeRepository : IBaseRepository<Office>
    {
        List<LocalSupply> GetSupply(int id);

        List<LocalSupply> GetSupply(Office of);

        List<LocalSupply> UpdateSupply(int id, List<LocalSupply> ls);

        List<LocalSupply> UpdateSupply(Office of, List<LocalSupply> ls);
    }
}
