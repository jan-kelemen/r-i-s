﻿using Apoteka.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.BLL.Repositories
{
    public interface ISupplierRepository : IBaseRepository<Supplier>
    {
        List<Medicine> GetMedicines(int id);

        List<Medicine> GetMedicines(Supplier eo);

        List<Medicine> UpdateMedicines(int id, List<Medicine> eom);

        List<Medicine> UpdateMedicines(Supplier eo, List<Medicine> eom);
    }
}
