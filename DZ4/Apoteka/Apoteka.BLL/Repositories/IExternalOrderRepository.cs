﻿using Apoteka.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.BLL.Repositories
{
    public interface IExternalOrderRepository : IBaseRepository<ExternalOrder>
    {
        ExternalOrder Create(ExternalOrder eo, List<ExternalOrderMedicine> eom);

        List<ExternalOrderMedicine> GetMedicines(int id);

        List<ExternalOrderMedicine> GetMedicines(ExternalOrder eo);

        List<ExternalOrderMedicine> UpdateMedicines(int id, List<ExternalOrderMedicine> eom);

        List<ExternalOrderMedicine> UpdateMedicines(ExternalOrder eo, List<ExternalOrderMedicine> eom);
    }
}
