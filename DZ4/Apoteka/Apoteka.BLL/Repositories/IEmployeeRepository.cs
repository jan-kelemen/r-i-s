﻿using Apoteka.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.BLL.Repositories
{
    public interface IEmployeeRepository : IBaseRepository<Employee>
    {
        EmployeeRole CreateRole(EmployeeRole er);

        EmployeeRole GetRole(int id);

        List<EmployeeRole> GetAllRoles();

        EmployeeRole UpdateRole(EmployeeRole er);

        bool DeleteRole(int id);

        bool DeleteRole(EmployeeRole er);
    }
}
