﻿using Apoteka.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.BLL.Repositories
{
    public interface IBaseRepository<T>
    {
        T Create(T e);

        T Get(int id);

        List<T> GetAll();

        T Update(T e);

        bool Delete(int id);

        bool Delete(T e);
    }
}
