﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.BLL.Models
{
    public class Perscription : ModelBase
    {
        public const int MAX_CARD_NUMBER_LENGTH = 11;

        private string cardNumber;

        private DateTime issuedOn;

        public Perscription(int id, string cardNumber, DateTime issuedOn) : base(id)
        {
            CardNumber = cardNumber;
            IssuedOn = issuedOn;
        }

        public string CardNumber
        {
            get { return cardNumber; }
            set
            {
                checkTextFieldExact(value, MAX_CARD_NUMBER_LENGTH, "Invalid card number value");
                cardNumber = value;
            }
        }

        public DateTime IssuedOn
        {
            get { return issuedOn; }
            set { issuedOn = value; }
        }
    }
}
