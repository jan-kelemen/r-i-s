﻿using Apoteka.BLL.Models;
using Apoteka.BLL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.BLL.Services
{
    public class ExternalOrderService
    {
        private IRepositoryContext context;

        public ExternalOrderService(IRepositoryContext context)
        {
            this.context = context;
        }

        public Tuple<ExternalOrder, List<ExternalOrderMedicine>> Create(ExternalOrder eo, List<ExternalOrderMedicine> med)
        {
            var order = context.ExternalOrders.Create(eo, med);
            var medicines = context.ExternalOrders.GetMedicines(order);

            return new Tuple<ExternalOrder, List<ExternalOrderMedicine>>(order, medicines);
        }

        public Tuple<ExternalOrder, List<ExternalOrderMedicine>> Get(int id)
        {
            var order = context.ExternalOrders.Get(id);
            var medicines = context.ExternalOrders.GetMedicines(order);

            return new Tuple<ExternalOrder, List<ExternalOrderMedicine>>(order, medicines);
        }

        public Tuple<ExternalOrder, List<ExternalOrderMedicine>> GetNth(int n = 0)
        {
            var order = context.ExternalOrders.GetAll()[n];
            var medicines = context.ExternalOrders.GetMedicines(order);

            return new Tuple<ExternalOrder, List<ExternalOrderMedicine>>(order, medicines);
        }

        public int Count()
        {
            return context.ExternalOrders.GetAll().Count;
        }

        public Tuple<ExternalOrder, List<ExternalOrderMedicine>> Update(ExternalOrder eo, List<ExternalOrderMedicine> med)
        {
            var order = context.ExternalOrders.Update(eo);
            var medicines = context.ExternalOrders.UpdateMedicines(eo, med);

            return new Tuple<ExternalOrder, List<ExternalOrderMedicine>>(order, medicines);
        }

        public bool Delete(int id)
        {
            return context.ExternalOrders.Delete(id);
        }
    }
}
