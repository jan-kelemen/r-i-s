var searchData=
[
  ['binomial',['binomial',['../namespacebioinf.html#a2edacd6ce5784a07df005d5103e180a6',1,'bioinf']]],
  ['bioinf',['bioinf',['../namespacebioinf.html',1,'']]],
  ['bioinf_5futility_2ecpp',['bioinf_utility.cpp',['../bioinf__utility_8cpp.html',1,'']]],
  ['bioinf_5futility_2eh',['bioinf_utility.h',['../bioinf__utility_8h.html',1,'']]],
  ['bits_5ffor_5foffset',['bits_for_offset',['../classbioinf_1_1_r_r_r_table.html#a0acdc653a6101799957e3182852ab18d',1,'bioinf::RRRTable']]],
  ['block_5flength',['block_length',['../classbioinf_1_1_r_r_r_sequence.html#a473cd0ed22f2e76030dedac04c87e0ab',1,'bioinf::RRRSequence']]],
  ['block_5ft',['block_t',['../classbioinf_1_1_r_r_r_table.html#a8367ded5a3fe193aa8f43cfd12cb7bc9',1,'bioinf::RRRTable']]],
  ['blocks_5fin_5fsuperblock',['blocks_in_superblock',['../classbioinf_1_1_r_r_r_sequence.html#a0ce230a8df94b13899ada5298fe2864d',1,'bioinf::RRRSequence']]]
];
