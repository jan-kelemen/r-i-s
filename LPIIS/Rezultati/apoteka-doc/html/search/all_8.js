var searchData=
[
  ['lastname',['LastName',['../class_apoteka_1_1_b_l_l_1_1_models_1_1_employee.html#a5beb8dc210af284b5286b9f60074c8ac',1,'Apoteka.BLL.Models.Employee.LastName()'],['../class_apoteka_1_1_d_a_l_1_1_model_1_1_employee.html#aa2a551cd8ce61a58c4eb03aa604edc8f',1,'Apoteka.DAL.Model.Employee.LastName()']]],
  ['localsupplies',['LocalSupplies',['../class_apoteka_1_1_d_a_l_1_1_model_1_1_medicine.html#ae6ab64ed6ed26db64e1cff3409e36bd3',1,'Apoteka.DAL.Model.Medicine.LocalSupplies()'],['../class_apoteka_1_1_d_a_l_1_1_model_1_1_office.html#a3672f08586eedd2bdb4c98cc720d56e3',1,'Apoteka.DAL.Model.Office.LocalSupplies()'],['../class_apoteka_1_1_d_a_l_1_1_model_1_1_r_i_s_entities.html#ac051b251dafb28263f96683ba6bea4f0',1,'Apoteka.DAL.Model.RISEntities.LocalSupplies()']]],
  ['localsupply',['LocalSupply',['../class_apoteka_1_1_b_l_l_1_1_models_1_1_local_supply.html',1,'Apoteka.BLL.Models.LocalSupply'],['../class_apoteka_1_1_d_a_l_1_1_model_1_1_local_supply.html',1,'Apoteka.DAL.Model.LocalSupply'],['../class_apoteka_1_1_b_l_l_1_1_models_1_1_local_supply.html#a8b97dab0edbe677bd449c27f324ef1ff',1,'Apoteka.BLL.Models.LocalSupply.LocalSupply()']]],
  ['localsupply_2ecs',['LocalSupply.cs',['../_apoteka_8_b_l_l_2_models_2_local_supply_8cs.html',1,'(Global Namespace)'],['../_apoteka_8_d_a_l_2_model_2_local_supply_8cs.html',1,'(Global Namespace)']]]
];
