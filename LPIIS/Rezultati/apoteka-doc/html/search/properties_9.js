var searchData=
[
  ['office',['Office',['../class_apoteka_1_1_b_l_l_1_1_models_1_1_employee.html#a620391b920c2a1d4233daf2321090530',1,'Apoteka.BLL.Models.Employee.Office()'],['../class_apoteka_1_1_d_a_l_1_1_model_1_1_employee.html#a8e0c38fd00771b69f0938e7cabaeb961',1,'Apoteka.DAL.Model.Employee.Office()'],['../class_apoteka_1_1_d_a_l_1_1_model_1_1_local_supply.html#ac08bd18422c718504f531cb4cf7330b7',1,'Apoteka.DAL.Model.LocalSupply.Office()']]],
  ['officeid',['OfficeId',['../class_apoteka_1_1_d_a_l_1_1_model_1_1_employee.html#aec3d6dbb43c630b4ba1f67eb29bd8d5b',1,'Apoteka.DAL.Model.Employee.OfficeId()'],['../class_apoteka_1_1_d_a_l_1_1_model_1_1_local_supply.html#a2058c8d3a40e8724dc254f686f7e2937',1,'Apoteka.DAL.Model.LocalSupply.OfficeId()']]],
  ['offices',['Offices',['../interface_apoteka_1_1_b_l_l_1_1_repositories_1_1_i_repository_context.html#a800d8ab586e15e4cafbf07043df11e49',1,'Apoteka.BLL.Repositories.IRepositoryContext.Offices()'],['../class_apoteka_1_1_d_a_l_1_1_model_1_1_r_i_s_entities.html#a0e6b345cc850140d67a32d644c5a2658',1,'Apoteka.DAL.Model.RISEntities.Offices()']]]
];
