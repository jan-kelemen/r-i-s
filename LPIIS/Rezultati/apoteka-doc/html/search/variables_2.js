var searchData=
[
  ['max_5faddress_5flength',['MAX_ADDRESS_LENGTH',['../class_apoteka_1_1_b_l_l_1_1_models_1_1_office.html#addaaba37b8c723cbad078a11b031aaa1',1,'Apoteka.BLL.Models.Office.MAX_ADDRESS_LENGTH()'],['../class_apoteka_1_1_b_l_l_1_1_models_1_1_supplier.html#a051b23aeec514779704efe830902b9da',1,'Apoteka.BLL.Models.Supplier.MAX_ADDRESS_LENGTH()']]],
  ['max_5fcard_5fnumber_5flength',['MAX_CARD_NUMBER_LENGTH',['../class_apoteka_1_1_b_l_l_1_1_models_1_1_perscription.html#a10190ef85490cbc4f7cff946e43ae86b',1,'Apoteka::BLL::Models::Perscription']]],
  ['max_5fdescription_5flegnth',['MAX_DESCRIPTION_LEGNTH',['../class_apoteka_1_1_b_l_l_1_1_models_1_1_medicine.html#a72045d33c1aa81506272f919c10c2da2',1,'Apoteka::BLL::Models::Medicine']]],
  ['max_5femail_5flength',['MAX_EMAIL_LENGTH',['../class_apoteka_1_1_b_l_l_1_1_models_1_1_employee.html#a2c23f403499e7871da6729f61a0bd1f9',1,'Apoteka::BLL::Models::Employee']]],
  ['max_5ffirst_5fname_5flength',['MAX_FIRST_NAME_LENGTH',['../class_apoteka_1_1_b_l_l_1_1_models_1_1_employee.html#ac5cb43bfcfe6fda91e43936e7298314b',1,'Apoteka::BLL::Models::Employee']]],
  ['max_5flast_5fname_5flength',['MAX_LAST_NAME_LENGTH',['../class_apoteka_1_1_b_l_l_1_1_models_1_1_employee.html#a19fb5c8df3cdef635ccaad836344008c',1,'Apoteka::BLL::Models::Employee']]],
  ['max_5fname_5flength',['MAX_NAME_LENGTH',['../class_apoteka_1_1_b_l_l_1_1_models_1_1_employee_role.html#a7b87a6749286d650fed5bd4691c4441a',1,'Apoteka.BLL.Models.EmployeeRole.MAX_NAME_LENGTH()'],['../class_apoteka_1_1_b_l_l_1_1_models_1_1_medicine.html#ae4df8185fa3209f21e9fa913baeaab58',1,'Apoteka.BLL.Models.Medicine.MAX_NAME_LENGTH()'],['../class_apoteka_1_1_b_l_l_1_1_models_1_1_office.html#a666572551293cb9443fd7bf0a1a74a2c',1,'Apoteka.BLL.Models.Office.MAX_NAME_LENGTH()'],['../class_apoteka_1_1_b_l_l_1_1_models_1_1_supplier.html#a4f250056e38b92c3134f1dcdc4eaf81c',1,'Apoteka.BLL.Models.Supplier.MAX_NAME_LENGTH()']]],
  ['medicines',['Medicines',['../class_apoteka_1_1_d_a_l_1_1_repositories_1_1_repository_context.html#a5338780f75818cae4bdf63f3d7ee2aa2',1,'Apoteka::DAL::Repositories::RepositoryContext']]]
];
