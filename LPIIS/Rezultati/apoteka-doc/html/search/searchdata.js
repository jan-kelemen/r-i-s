var indexSectionsWithContent =
{
  0: "abcdefgilmnoprstu",
  1: "bcefilmoprs",
  2: "a",
  3: "abcdefgilmoprs",
  4: "abcdegilmoprstu",
  5: "demops",
  6: "acdefilmnoprst"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "properties"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Properties"
};

