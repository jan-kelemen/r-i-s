#include "RRRSequence.h"

#include <algorithm>
#include <bitset>
#include <cmath>

#ifndef NDEBUG
#include <iostream>
#endif // !NDEBUG


#include "bioinf_utility.h"


namespace bioinf {
    RRRSequence::RRRSequence(std::string const& sequence_str)
        : sequence_length_{sequence_str.size()}
        , total_blocks_{0}
        , coded_sequence_({0}) {

        calculate_lengths(sequence_str.size());

        size_t coded_index = 0;
        int8_t filled_bits = 0;

        size_t total_rank = 0;

        size_t superblock_size = blocks_in_superblock_;
        superblock_size *= block_length_;

        size_t superblock_count = sequence_length_ / superblock_size + ((sequence_length_ % superblock_size) ? 1 : 0);

        //for each superblock
        for (size_t si = 0; si < superblock_count; ++si) {

            superblocks_.emplace_back(total_rank, coded_index * 64 + filled_bits + class_length_);

            //for each block in superblock
            for (size_t bj = 0; bj < blocks_in_superblock_; ++bj) {
                auto block_start = si * superblock_size + bj * block_length_;
                if (block_start >= sequence_str.length()) { break; }

                int32_t block = 0;
                uint32_t mask = 1 << (block_length_ - 1);

                auto i = 0;
                for (auto it = sequence_str.begin() + block_start;
                     block_start + i < sequence_str.length() && i < block_length_; ++it, ++i) {

                    if (*it == '1') {
                        block |= mask;
                    }
                    mask >>= 1;
                }

                auto rrr_class = popcount(block);
                uint64_t offset = RRRTable::offset_of(block, rrr_class);
                auto bits_for_offset = RRRTable::bits_for_offset(block_length_, rrr_class);

                total_rank += rrr_class;

                uint64_t temp = rrr_class;
                temp <<= bits_for_offset;
                temp |= offset;

                int8_t temp_bits = static_cast<int8_t>(class_length_) + bits_for_offset;

                auto shift_value = 64 - filled_bits - temp_bits;
                if (shift_value >= 0) {
                    coded_sequence_[coded_index] |= temp << shift_value;
                    filled_bits += temp_bits;
                }
                else {
                    coded_sequence_[coded_index] |= temp >> std::abs(shift_value);
                    coded_sequence_.emplace_back(temp << (128 - temp_bits - filled_bits));
                    ++coded_index;
                    filled_bits = temp_bits + filled_bits - 64;
                }

                ++total_blocks_;
            }
        }

        total_length_ = coded_index * 64 + filled_bits;
    }

    size_t RRRSequence::rank0(size_t index) const {
        return index + 1 - rank1(index);
    }

    size_t RRRSequence::rank1(size_t index) const {
        size_t ib = index / block_length_;
        size_t is = ib / blocks_in_superblock_;
        auto sum = superblocks_[is].first;
        auto class_start = superblocks_[is].second - class_length_;
        auto block_index = is * blocks_in_superblock_;

        while (ib != block_index) {
            auto class_ = extract_bits<RRRTable::class_t>(coded_sequence_.begin(), class_start, class_length_);
            sum += class_;
            ++block_index;
            class_start += class_length_ + RRRTable::bits_for_offset(block_length_, class_);
        }

        auto class_ = extract_bits<RRRTable::class_t>(coded_sequence_.begin(), class_start, class_length_);
        auto bits_for_offset = RRRTable::bits_for_offset(block_length_, class_);
        auto offset = extract_bits<size_t>(coded_sequence_.begin(), class_start + class_length_, bits_for_offset);
        auto position = index % block_length_;
        sum += RRRTable::rank_for(class_, offset, block_length_, position);

        return sum;
    }

    size_t RRRSequence::select0(size_t count) const {
        auto is = select0_superblock(count);

        auto rv = is * block_length_ * blocks_in_superblock_;
        auto sum = rv - superblocks_[is].first;

        auto class_start = superblocks_[is].second - class_length_;
        auto block_index = is * blocks_in_superblock_;

        while (block_index < total_blocks_) {
            auto class_ = extract_bits<RRRTable::class_t>(coded_sequence_.begin(), class_start, class_length_);
            if (sum + block_length_ - class_ < count) {
                sum += block_length_ - class_;
            }
            else {
                break;
            }
            rv += block_length_;
            class_start += class_length_ + RRRTable::bits_for_offset(block_length_, class_);
            ++block_index;
        }

        auto class_ = extract_bits<RRRTable::class_t>(coded_sequence_.begin(), class_start, class_length_);
        auto bits_for_offset = RRRTable::bits_for_offset(block_length_, class_);
        auto offset = extract_bits<size_t>(coded_sequence_.begin(), class_start + class_length_, bits_for_offset);

        rv += RRRTable::index_for_rank0(class_, offset, block_length_, static_cast<int8_t>(count - sum));
        return rv;
    }

    size_t RRRSequence::select1(size_t count) const {
        std::pair<size_t, size_t> temp_pair{count, 0};
        auto superblock_it = std::lower_bound(superblocks_.begin(), superblocks_.end(), temp_pair);
        auto it_distance = std::distance(superblocks_.begin(), superblock_it);
        auto is = static_cast<size_t>(std::max<int64_t>(0, it_distance - 1));

        auto sum = superblocks_[is].first;
        auto rv = is * blocks_in_superblock_ * block_length_;

        auto class_start = superblocks_[is].second - class_length_;
        auto block_index = is * blocks_in_superblock_;

        while (block_index < total_blocks_) {
            auto class__ = extract_bits<RRRTable::class_t>(coded_sequence_.begin(), class_start, class_length_);
            if (sum + class__ < count) {
                sum += class__;
            }
            else {
                break;
            }
            rv += block_length_;
            class_start += class_length_ + RRRTable::bits_for_offset(block_length_, class__);
        }

        auto class_ = extract_bits<RRRTable::class_t>(coded_sequence_.begin(), class_start, class_length_);
        auto bits_for_offset = RRRTable::bits_for_offset(block_length_, class_);
        auto offset = extract_bits<size_t>(coded_sequence_.begin(), class_start + class_length_, bits_for_offset);

        rv += RRRTable::index_for_rank1(class_, offset, block_length_, static_cast<int8_t>(count - sum));
        return rv;
    }

    char RRRSequence::access(size_t index) const {
        if (index == 0) {
            return  static_cast<char>(rank1(index));
        }
        return static_cast<char>(rank1(index) - rank1(index - 1));
    }

    uint8_t RRRSequence::block_length() const {
        return block_length_;
    }

    uint8_t RRRSequence::blocks_in_superblock() const {
        return blocks_in_superblock_;
    }

    uint8_t RRRSequence::class_length() const {
        return class_length_;
    }

    size_t RRRSequence::sequence_length() const {
        return sequence_length_;
    }

    void RRRSequence::calculate_lengths(size_t length) {
        auto logn = std::log2(length);

        auto s = std::pow(std::floor(logn), 2);
        auto b = std::floor(logn / 2);

        block_length_ = static_cast<decltype(block_length_)>(std::max<double>(b, 1));
        blocks_in_superblock_ = static_cast<decltype(blocks_in_superblock_)>(s / block_length_);
        class_length_ = static_cast<decltype(class_length_)>(std::ceil(std::log2(block_length_ + 1)));
    }

    size_t RRRSequence::select0_superblock(size_t count) const {
        if (superblocks_.size() == 1) {
            return 0;
        }


        int low = 1;
        int high = superblocks_.size() - 1;
        while (low <= high) {
            auto mid = (low + high) / 2;
            auto value = mid * blocks_in_superblock_ * block_length_ - superblocks_[mid].first;
            if (value >= count) {
                high = mid - 1;
            }
            else {
                low = mid + 1;
            }
        }

        return low - 1;
    }

    std::string to_string(RRRSequence const& sequence) {
        std::string rv;

        for (auto& v : sequence.coded_sequence_) {
            std::bitset<64> bs(v);
            rv += bs.to_string();
        }

        return rv.substr(0, sequence.total_length_);
    }
}

