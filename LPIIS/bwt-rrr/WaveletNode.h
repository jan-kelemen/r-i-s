#ifndef WAVELET_NODE_H
#define WAVELET_NODE_H

#include "WaveletTreeCommon.h"
#include "RRRSequence.h"

namespace bioinf {

    /**
     * \brief WaveletNode is a node of a binary wavelet tree implemented using RRR structure.
     *
     * \author Jan Kelemen
     * \version 1.0
     */
    class WaveletNode {
    public:
        // - - - Constructors

        /**
         * \brief Class constructor, initializes the wavelet node.
         * \param[in] sequence The sequence for which the node is created.
         * \param[in] alphabet The alphabet of the tree.
         * \param[in] alphabet_begin The index at which the alphabet of this node begins.
         * \param[in] alphabet_end The index to one past the end at which the alphabet of this node ends.
         * \param[in] parent The parent of the node.
         * \since alpha
         *
         * \throws std::bad_alloc If it runs out of memory.
         */
        WaveletNode(std::string const& sequence, alphabet_t const& alphabet, 
                    size_t alphabet_begin, size_t alphabet_end, WaveletNode const* parent = nullptr);

        /**
         * \brief Copy constructor.
         * \since 1.0
         */
        WaveletNode(WaveletNode const&);

        /**
         * \brief Move constructor
         * \since 1.0
         */
        WaveletNode(WaveletNode&&) = default;

        // - - - Operator overloads

        /**
         * \brief Copy assignment.
         * \since 1.0
         */
        WaveletNode& operator=(WaveletNode const&);

        /**
         * \brief Move assignment.
         * \since 1.0
         */
        WaveletNode& operator=(WaveletNode&&) = default;

        // - - - Member functions

        /**
         * \brief Calculates the rank of 0 in the range [0, index].
         * \param[in] index The index.
         * \return Number of occurrences of 0 in the node in specified range.
         * \since alpha
         */
        size_t rank0(size_t index) const;

        /**
         * \brief Calculates the rank of 1 in the range [0, index].
         * \param[in] index The index.
         * \return Number of occurrences of 1 in the node in specified range.
         * \since alpha
         */
        size_t rank1(size_t index) const;

        /**
         * \brief Calculates the index at which 0 appears in the node for the count-th time.
         * \param[in] count The count.
         * \return Index of the count-th appearence.
         * \since alpha
         */
        size_t select0(size_t count) const;

        /**
         * \brief Calculates the index at which 1 appears in the node for the count-th time.
         * \param[in] count The count.
         * \return Index of the count-th appearence.
         * \since alpha
        */
        size_t select1(size_t count) const;

        /**
         * \brief Returns the character of the sequence at specified index.
         * \param[in] index The index.
         * \return The bit.
         * \since alpha
         */
        char access(size_t index) const;

        // - - - Accessors
        /**
         * \return Index to one past the end in the alphabet for the characters that are encoded with 0.
         * \since alpha
         */
        size_t zero_encoded_to_index() const;

        /**
         * \return Index to the beggining of the alphabet of the node.
         * \since alpha
         */
        size_t alphabet_begin_index() const;

        /**
         * \return Index to the end of the alphabet of the node.
         * \since alpha
         */
        size_t alphabet_end_index() const;

        /**
         * \return Parent node of this node.
         * \since alpha
         */
        WaveletNode const* parent() const;

        /**
         * \return Left child of this node.
         * \since alpha
         */
        WaveletNode const* left() const;

        /**
         * \return Right child of this node.
         * \since alpha
         */
        WaveletNode const* right() const;

        // - - - Destructor

        /**
         * \brief Class destructor.
         * \since alpha
         */
        ~WaveletNode();
    private:
        RRRSequence sequence_;

        WaveletNode const* parent_;
        WaveletNode const* left_;
        WaveletNode const* right_;

        size_t zero_encoded_to_;
        size_t a_begin_;
        size_t a_end_;
    };
}
#endif // !WAVELET_NODE_H
