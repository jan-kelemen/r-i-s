#ifndef RRR_TABLE_H
#define RRR_TABLE_H

#include <array>
#include <cstdint>
#include <vector>

namespace bioinf {

    /**
     * \brief RRRTable is one of the data structures used for RRR data structure implementation.
     *
     * \author Marina Brebric
     * \version alpha
     */
    class RRRTable {
    public:
        using class_t = int8_t;
        using block_t = size_t;
        using rank_vector_t = std::vector<int8_t>;

        /**
         * \brief Initializes the RRRTable to a table of specified block size.
         * \param[in] block_size The size of the block.
         * \since alpha
         */
        static void initialize_for_block_size(int8_t block_size);

        /**
         * \brief Calculates the offset of the block.
         * \param[in] block The block.
         * \param[in] value Class value of the block.
         * \since alpha
         */
        static size_t offset_of(block_t const& block, class_t const& value);

        /**
         * \brief Calculates number of bits needed for the offset
         * \param[in] b The block size.
         * \param[in] c The class value.
         * \since alpha
         */
        static int8_t bits_for_offset(uint8_t const& b, class_t const& c);

        /**
         * \brief Calculates the rank of the bit at given position in the specified block
         * \param[in] c The class of the block.
         * \param[in] offset The offset of the block.
         * \param[in] block_size The block size.
         * \param[in] position The position of the bit.
         * \since alpha
         */
        static int8_t rank_for(class_t const& c, size_t const& offset, int8_t const& block_size, size_t const& position);

        /**
         * \brief Calculates the index at which rank for 0 appears in the block
         * \param[in] c The class of the block.
         * \param[in] offset The offset of the block.
         * \param[in] block_size The block size.
         * \param[in] rank The searched rank.
         * \since alpha
         *
         * \throws std::logic_error If searched rank doesn't exist in the block.
         */
        static int8_t index_for_rank0(class_t const& c, size_t const& offset, int8_t const& block_size, int8_t const& rank);

        /**
         * \brief Calculates the index at which rank for 1 appears in the block
         * \param[in] c The class of the block.
         * \param[in] offset The offset of the block.
         * \param[in] block_size The block size.
         * \param[in] rank The searched rank.
         * \since alpha
         *
         * \throws std::logic_error If searched rank doesn't exist in the block.
         */
        static int8_t index_for_rank1(class_t const& c, size_t const& offset, int8_t const& block_size, int8_t const& rank);
    private:
        using table_t = std::vector<std::vector<std::pair<block_t, rank_vector_t>>>;

        RRRTable() = delete;
        RRRTable(RRRTable const&) = delete;
        RRRTable& operator=(RRRTable const&) = delete;
        ~RRRTable() = delete;

        static table_t table_;

        static int8_t block_size_;
        static std::array<int8_t, sizeof(size_t) * 8 + 1> bits_for_offset_;
    };
}
#endif // !RRR_TABLE_H
