#include "bioinf_utility.h"

#include <fstream>
#include <numeric>
#include <stdexcept>

#ifdef __linux__ 
#include "stdlib.h"
#include "stdio.h"
#else
#include "windows.h"
#include "psapi.h"
#endif

namespace bioinf {

    std::string parse_fasta(char const* filename) {
        std::ifstream file(filename);

        if (!file.is_open()) {
            throw std::invalid_argument(std::string("File not found: ") + filename);
        }

        std::string content;
        std::string line;
        std::getline(file, line); // ignore first line with block name
        while (std::getline(file, line)) {
            if (line[0] == ',') { continue; } //skip comments
            //TODO check if it's neccessary to trim the string 
            line.erase(line.find_last_not_of(" \n\r\t") + 1);
            content += line;
        }

        return content;
    }

    std::pair<double, double> calculate_memory_usage() {
        static auto initial_memory = .0;

#ifdef __linux__ 
        FILE* file = fopen("/proc/self/status", "r");
        int result = -1;
        char line[128];

        while (fgets(line, 128, file) != NULL) {
            if (strncmp(line, "VmRSS:", 6) == 0) {
                int i = strlen(line);
                while (*line < '0' || *line > '9') line++;
                line[i - 3] = '\0';
                result = atoi(line);
                break;
            }
        }
        fclose(file);
        double current = result / 1024.;
#else
        PROCESS_MEMORY_COUNTERS pmc;
        GetProcessMemoryInfo(GetCurrentProcess(), &pmc, sizeof(pmc));

        SIZE_T physMemUsedByMe = pmc.WorkingSetSize;
        double current = physMemUsedByMe / (1024. * 1024);
#endif
        if (initial_memory > 0) {
            return{current, current - initial_memory};
        }

        initial_memory = current;
        return{current, 0};
    }

    std::tuple<int64_t, double, double, double> calculate_statistics(Statistics const& stats) {
        auto avg = [](std::vector<int64_t> const& v) {
            return std::accumulate(v.begin(), v.end(), .0) / v.size();
        };

        auto access = avg(stats.access_times);
        auto rank = avg(stats.rank_times);
        auto select = avg(stats.select_times);

        return{stats.tree_construction_time, access, rank, select};
    }
}
