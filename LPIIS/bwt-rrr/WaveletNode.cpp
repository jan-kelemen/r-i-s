#include "WaveletNode.h"

#include <algorithm>
#include <iterator>
#include <unordered_set>

#include <iostream>

#include "WaveletTree.h"

namespace bioinf {
    WaveletNode::WaveletNode(std::string const& sequence, alphabet_t const& alphabet, size_t alphabet_begin, size_t alphabet_end, WaveletNode const* parent)
        : parent_{parent}
        , zero_encoded_to_{alphabet_begin + (alphabet_end - alphabet_begin + 1) / 2}
        , a_begin_{alphabet_begin}
        , a_end_{alphabet_end - 1} {

        auto alphabet_mid = zero_encoded_to_;

        std::string left_seq;
        left_seq.reserve(sequence.size() / 2);
        std::unordered_set<char> left_alphabet;

        std::string right_seq;
        right_seq.reserve(sequence.size() / 2);
        std::unordered_set<char> right_alphabet;

        std::string rrr_str;
        rrr_str.reserve(sequence.size());

        for (auto& sym : sequence) {
            auto is_zero = sym < alphabet[alphabet_mid];

            if (is_zero) {
                left_seq.push_back(sym);
                left_alphabet.insert(sym);
                rrr_str.push_back('0');
            }
            else {
                right_seq.push_back(sym);
                right_alphabet.insert(sym);
                rrr_str.push_back('1');
            }
        }

        sequence_ = RRRSequence(rrr_str);

        try {
            if (left_alphabet.size() >= 2) {
                left_ = new WaveletNode(left_seq, alphabet, alphabet_begin, alphabet_mid, this);
            }
            if (right_alphabet.size() >= 2) {
                right_ = new WaveletNode(right_seq, alphabet, alphabet_mid, alphabet_end, this);
            }
        }
        catch (std::bad_alloc&) {
            delete left_;
            delete right_;

            throw;
        }
    }

    WaveletNode::WaveletNode(WaveletNode const& n) {
        sequence_ = n.sequence_;

        parent_ = nullptr;
        if (n.left_ != nullptr) {
            left_ = new WaveletNode(*n.left_);
        }
        if (n.right_ != nullptr) {
            right_ = new WaveletNode(*n.right_);
        }

        zero_encoded_to_ = n.zero_encoded_to_;
        a_begin_ = n.a_begin_;
        a_end_ = n.a_end_;
    }

    WaveletNode& WaveletNode::operator=(WaveletNode const& n) {
        if (&n == this) { return *this; }

        sequence_ = n.sequence_;

        //upper levels of the tree stay the same so there is no need to change the parent node
        if (n.left_ != nullptr) {
            auto temp = new WaveletNode(*n.left_);
            temp->parent_ = this;
            delete left_;
            left_ = temp;
        }
        else {
            delete left_;
        }

        if (n.right_ != nullptr) {
            auto temp = new WaveletNode(*n.right_);
            temp->parent_ = this;
            delete right_;
            right_ = temp;
        }
        else {
            delete right_;
        }

        zero_encoded_to_ = n.zero_encoded_to_;
        a_begin_ = n.a_begin_;
        a_end_ = n.a_end_;

        return *this;
    }

    size_t WaveletNode::zero_encoded_to_index() const {
        return zero_encoded_to_;
    }

    size_t WaveletNode::alphabet_begin_index() const {
        return a_begin_;
    }

    size_t WaveletNode::alphabet_end_index() const {
        return a_end_;
    }

    size_t WaveletNode::rank0(size_t index) const {
        return sequence_.rank0(index);
    }

    size_t WaveletNode::rank1(size_t index) const {
        return sequence_.rank1(index);
    }

    size_t WaveletNode::select0(size_t index) const {
        return sequence_.select0(index);
    }

    size_t WaveletNode::select1(size_t index) const {
        return sequence_.select1(index);
    }

    char WaveletNode::access(size_t index) const {
        return sequence_.access(index);
    }

    WaveletNode const* WaveletNode::parent() const {
        return parent_;
    }

    WaveletNode const* WaveletNode::left() const {
        return left_;
    }

    WaveletNode const* WaveletNode::right() const {
        return right_;
    }

    WaveletNode::~WaveletNode() {
        delete left_;
        delete right_;
    }
}

