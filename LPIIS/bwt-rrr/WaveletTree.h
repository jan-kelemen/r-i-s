#ifndef WAVELET_TREE_H
#define WAVELET_TREE_H

#include <vector>
#include <string>
#include <array>

#include "WaveletTreeCommon.h"

namespace bioinf {

    class WaveletNode;

    /**
     * \brief WaveletTree is a binary wavelet tree implemented using RRR structure.
     *
     * \author Jan Kelemen
     * \version 1.0
     */
    class WaveletTree {
    public:
        // - - - Constructors

        /**
         * \brief Class constructor, initializes the wavelet tree.
         * \param[in] sequence The sequence for which the tree is created.
         * \since alpha
         */
        explicit WaveletTree(std::string const& sequence);

        /**
         * \brief Copy constructor.
         * \since 1.0
         */
        WaveletTree(WaveletTree const&) = default;

        /**
         * \brief Move constructor
         * \since 1.0
         */
        WaveletTree(WaveletTree&&) = default;

        // - - - Operator overloads

        /**
         * \brief Copy assignment.
         * \since 1.0
         */
        WaveletTree& operator=(WaveletTree const&) = default;

        /**
         * \brief Move assignment.
         * \since 1.0
         */
        WaveletTree& operator=(WaveletTree&&) = default;

        // - - - Member functions

        /**
         * \brief Calculates the rank of the character in the sequence in range [0, index].
         * \param[in] c The character.
         * \param[in] index The index.
         * \return Number of occurrences of the character.
         * \since alpha
         */
        size_t rank(char c, size_t index) const;

        /**
         * \brief Calculates the index at which the character appears in the sequence for the count-th time.
         * \param[in] c The character.
         * \param[in] count The count.
         * \return Index of the count-th appearence.
         * \since alpha
         */
        size_t select(char c, size_t count) const;

        /**
         * \brief Returns the character of the sequence at specified index.
         * \param[in] index The index.
         * \return The character.
         * \since alpha
         */
        char access(size_t index) const;

        // - - - Destructor

        /**
         * \brief Class destructor.
         * \since alpha
         */
        ~WaveletTree();
    private:
        alphabet_t alphabet_; // alphabet for the tree
        std::array<int8_t, 256> alphabet_indexes_;
        WaveletNode const* root_; // root of the tree


        // Returns the node containing the character
        WaveletNode const* node_for_symbol(char c, size_t const& char_index) const;
    };
}
#endif // !WAVELET_TREE_H
