#ifndef BIOINF_UTILITY_H
#define BIOINF_UTILITY_H

#include <bitset>
#include <string>
#include <tuple>
#include <vector>

namespace bioinf {

    /**
     * \brief parses a file specified by the filename.
     * If a file contains multiple DNA sequences they will be ignored.
     *
     * \param[in] filename Name of the file
     * \return Parsed DNA sequence
     *
     * \throws std::invalid_argument If file couldn't be opened.
     */
    
    struct Statistics {
        int64_t tree_construction_time;
        std::vector<int64_t> access_times;
        std::vector<int64_t> rank_times;
        std::vector<int64_t> select_times;
    };
    
    std::string parse_fasta(char const* filename);

    /**
     * \breif Calculates the number of set bits in the passed value.
     * \param[in] value The value.
     * \return Number of set bits.
     */
    template<typename T>
    int8_t popcount(T const& value) {
        std::bitset<sizeof(T) * 8> bits(value);
        //maximum value is 64 so int8_t is ok
        return static_cast<int8_t>(bits.count());
    }

    /**
     * \brief Extracts the specified number of bits from the sequence of 64 bit integers.
     * \param[in] begin Beggining of the sequence.
     * \param[in] index Bit at which the bits for extraction start.
     * \param[in] length Number of bits to extract.
     * \return Extracted bits.
     */
    template<typename T, typename RIter>
    T extract_bits(RIter begin, size_t index, uint8_t length) {
        uint64_t rv = 0;
        uint64_t mask = (static_cast<uint64_t>(1) << length) - 1;

        auto end = index + length;
        if (!(index / 64 != end / 64 && end % 64 != 0)) {
            auto shift = 64 - index % 64 - length;
            rv |= (*(begin + index / 64) & (mask << shift)) >> shift;
        }
        else {
            auto first = *(begin + index / 64);
            auto second = *(begin + end / 64);

            auto bits_from_first = 64 - index % 64;
            auto leftover = length - bits_from_first;

            auto extracted_from_first = first & (mask >> leftover);
            auto extracted_from_second = second & (mask << (64 - leftover));

            extracted_from_first <<= leftover;
            extracted_from_second >>= (64 - leftover);

            rv = extracted_from_first | extracted_from_second;
        }

        return static_cast<T>(rv);
    }


    /**
     * \brief Calculates the memory usage of the program.
     * \return First number is the memory used by the entire program, second number is the memory used by the wavelet tree.
     * \author Denis Causevic, Hajrudin Coralic
     */
    std::pair<double, double> calculate_memory_usage();

    /**
     * \brief Calculates runtime statistics for tree operations.
     * \param[in] stats The operation runtimes.
     * \return tree construction time, average times for access, rank, select operations
     */
    std::tuple<int64_t, double, double, double> calculate_statistics(Statistics const& stats);

    template<typename T>
    size_t factorial(T num) {
        if (num == 0) { return 1; }

        size_t rv = 1;
        for (auto i = num; i > 1; --i) {
            rv *= i;
        }
        return rv;
    }

    template<typename T, typename U>
    size_t binomial(T const& n, U const& k) {
        return factorial(n) / (factorial(k) * factorial(n - k));
    }
}

#endif // !BIOINF_UTILITY_H
