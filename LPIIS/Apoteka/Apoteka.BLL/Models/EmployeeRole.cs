﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.BLL.Models
{
    public class EmployeeRole : ModelBase
    {
        public const int MAX_NAME_LENGTH = 80;

        private string name;

        public EmployeeRole(int id, string name) : base(id)
        {
            Name = name;
        }

        public string Name
        {
            get { return name; }
            set
            {
                checkTextField(value, MAX_NAME_LENGTH, "Invalid employee role name value");
                name = value;
            }
        }
    }
}
