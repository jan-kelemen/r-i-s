﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.BLL.Models
{
    public class Medicine : ModelBase
    {
        public const int MAX_NAME_LENGTH = 80;

        public const int MAX_DESCRIPTION_LEGNTH = 200;

        private string name;

        private string description;

        public Medicine(int id, string name, string description) : base(id)
        {
            Name = name;
            Description = description;
        }

        public string Name
        {
            get { return name; }
            set
            {
                checkTextField(value, MAX_NAME_LENGTH, "Invalid medicine name");
                name = value;
            }
        }

        public string Description
        {
            get { return description; }
            set
            {
                checkTextField(value, MAX_DESCRIPTION_LEGNTH, "Invalid medicine description value");
                description = value;
            }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
