﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.BLL.Models
{
    public class ExternalOrderMedicine : ModelBase
    {
        private Medicine medicine;

        private int amount;

        public ExternalOrderMedicine(int id, Medicine medicine, int amount) : base(id)
        {
            Medicine = medicine;
            Amount = amount;
        }

        public Medicine Medicine
        {
            get { return medicine; }
            set
            {
                if (value == null)
                {
                    throw new ApplicationException("Invalid external order medicine value");
                }
                medicine = value;
            }
        }

        public int Amount
        {
            get { return amount; }
            set
            {
                if (value <= 0)
                {
                    throw new ApplicationException("Invalid external order medicine amount value");
                }
                amount = value;
            }
        }
    }
}
