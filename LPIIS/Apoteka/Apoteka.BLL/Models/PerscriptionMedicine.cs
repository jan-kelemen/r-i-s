﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.BLL.Models
{
    public class PerscriptionMedicine : ModelBase
    {
        private Medicine medicine;

        private int amount;

        public PerscriptionMedicine(int id, Medicine medicine, int amount) : base(id)
        {
            Medicine = medicine;
            Amount = amount;
        }

        public Medicine Medicine
        {
            get { return medicine; }
            set
            {
                if(medicine == null)
                {
                    throw new ApplicationException("Invalid perscription medicine value");
                }
                medicine = value;
            }
        }

        public int Amount
        {
            get { return amount; }
            set
            {
                if(value <= 0)
                {
                    throw new ApplicationException("Invalid perscription medicine amount value");
                }
                amount = value;
            }
        }
    }
}
