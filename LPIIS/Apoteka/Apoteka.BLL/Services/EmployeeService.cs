﻿using Apoteka.BLL.Models;
using Apoteka.BLL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.BLL.Services
{
    public class EmployeeService
    {
        private IRepositoryContext context;

        public EmployeeService(IRepositoryContext context)
        {
            this.context = context;
        }

        public Employee Get(int id)
        {
            return context.Employees.Get(id);
        }

        public List<Employee> GetAll()
        {
            return context.Employees.GetAll();
        }
    }
}
