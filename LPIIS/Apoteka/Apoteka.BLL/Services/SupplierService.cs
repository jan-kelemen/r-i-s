﻿using Apoteka.BLL.Models;
using Apoteka.BLL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.BLL.Services
{
    public class SupplierService
    {
        private IRepositoryContext context;

        public SupplierService(IRepositoryContext context)
        {
            this.context = context;
        }

        public Supplier Get(int id)
        {
            return context.Suppliers.Get(id);
        }

        public List<Supplier> GetAll()
        {
            return context.Suppliers.GetAll();
        }
    }
}
