﻿using Apoteka.BLL.Models;
using Apoteka.BLL.Services;
using Apoteka.DAL.Repositories;
using Apoteka.Web.ViewModels.ExternalOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Apoteka.Web.Controllers
{
    public class ExternalOrderController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction(nameof(Details), new { n = 0 });
        }

        public ActionResult Details(int n)
        {
            using (var context = new RepositoryContext())
            {
                var orderService = new ExternalOrderService(context);

                var data = orderService.GetNth(n);

                var order = data.Item1;
                var medicines = data.Item2;

                var vm = new MasterDetail
                {
                    Current = n,
                    Total = orderService.Count(),

                    Id = order.Id,
                    IssuedOn = order.IssuedOn,
                    EmployeeName = order.Employee?.ToString() ?? "Unknown",
                    SupplierName = order.Supplier.ToString(),
                    Medicines = new List<MasterDetail.Medicine>(),
                };
                foreach(var m in medicines)
                {
                    vm.Medicines.Add(new MasterDetail.Medicine
                    {
                        MedicineName = m.Medicine.ToString(),
                        Amount = m.Amount,
                    });
                }

                return View(vm);
            }
        }

        public ActionResult Create()
        {
            using (var context = new RepositoryContext())
            {
                var supplierService = new SupplierService(context);
                var medicineService = new MedicineService(context);
                var employeeService = new EmployeeService(context);

                var vm = new CreateOrder
                {
                    Medicines = medicineService.GetAll(),
                    Suppliers = supplierSelectList(supplierService.GetAll()),
                    Employees = employeeSelectList(employeeService.GetAll()),
            };

                return View(vm);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateOrder vm)
        {
            using (var context = new RepositoryContext())
            {
                var supplierService = new SupplierService(context);
                var medicineService = new MedicineService(context);
                var employeeService = new EmployeeService(context);

                vm.Medicines = medicineService.GetAll();
                vm.Suppliers = supplierSelectList(supplierService.GetAll());
                vm.Employees = employeeSelectList(employeeService.GetAll());

                try
                {
                    var supplier = supplierService.Get(vm.SelectedSupplierId);
                    var employee = employeeService.Get(vm.SelectedEmployeeId);
                    var order = new ExternalOrder(0, vm.IssuedOn, supplier, employee);

                    var medicines = new List<ExternalOrderMedicine>();
                    foreach(var m in vm.MedicineEditors)
                    {
                        if(m.SelectedMedicineId != -1)
                        {
                            var medicine = medicineService.Get(m.SelectedMedicineId);
                            medicines.Add(new ExternalOrderMedicine(0, medicine, m.Amount));
                        }
                        else
                        {
                            throw new ApplicationException("Medicine not selected");
                        }
                    }

                    var orderService = new ExternalOrderService(context);
                    var data = context.ExternalOrders.Create(order, medicines);

                    return RedirectToAction(nameof(Details), new { n = orderService.Count() - 1 });
                }
                catch (ApplicationException e)
                {
                    ModelState.AddModelError("", e.Message);
                    return View(vm);
                }
            }
        }

        public ActionResult Edit(int n)
        {
            using (var context = new RepositoryContext())
            {
                var orderService = new ExternalOrderService(context);
                var employeeService = new EmployeeService(context);
                var supplierService = new SupplierService(context);
                var medicineService = new MedicineService(context);

                var data = orderService.GetNth(n);

                var order = data.Item1;
                var medicines = data.Item2;

                var vm = new EditOrder
                {
                    Current = n,
                    Total = orderService.Count(),

                    Id = order.Id,
                    IssuedOn = order.IssuedOn,
                    SelectedEmployeeId = order.Employee?.Id ?? -1,
                    SelectedSupplierId = order.Supplier.Id,
                    Employees = employeeSelectList(employeeService.GetAll()),
                    Suppliers = supplierSelectList(supplierService.GetAll()),
                    Medicines = medicineService.GetAll(),
                };
                foreach (var m in medicines)
                {
                    vm.MedicineEditors.Add(new EditOrder.MedicineEditor
                    {
                        Id = m.Id,
                        SelectedMedicineId = m.Medicine.Id,
                        Amount = m.Amount,
                        State = 'e',
                    });
                }

                return View(vm);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EditOrder vm)
        {
            using (var context = new RepositoryContext())
            {
                var supplierService = new SupplierService(context);
                var medicineService = new MedicineService(context);
                var employeeService = new EmployeeService(context);
                var orderService = new ExternalOrderService(context);

                vm.Medicines = medicineService.GetAll();
                vm.Suppliers = supplierSelectList(supplierService.GetAll());
                vm.Employees = employeeSelectList(employeeService.GetAll());

                try
                {
                    var supplier = supplierService.Get(vm.SelectedSupplierId);
                    var employee = employeeService.Get(vm.SelectedEmployeeId);
                    var order = orderService.Get(vm.Id).Item1;
                    order.IssuedOn = vm.IssuedOn;
                    order.Supplier = supplier;
                    order.Employee = employee;

                    var medicines = new List<ExternalOrderMedicine>();
                    foreach (var m in vm.MedicineEditors)
                    {
                        if(m.State != 'd')
                        {
                            var medicine = medicineService.Get(m.SelectedMedicineId);
                            medicines.Add(new ExternalOrderMedicine(0, medicine, m.Amount));
                        }
                    }

                    var data = orderService.Update(order, medicines);

                    return RedirectToAction(nameof(Details), new { n = vm.Current });
                }
                catch (ApplicationException e)
                {
                    ModelState.AddModelError("", e.Message);
                    return View(vm);
                }
            }
        }

        public ActionResult Delete(int id, int prev)
        {
            using (var context = new RepositoryContext())
            {
                var service = new ExternalOrderService(context);
                service.Delete(id);

                return RedirectToAction(nameof(Details), new { n = prev });
            }
        }

        private SelectList employeeSelectList(List<Employee> es)
        {
            return new SelectList((from e in es
                                     select new SelectListItem
                                     {
                                         Value = e.Id.ToString(),
                                         Text = e.ToString(),
                                     }).ToList(), "Value", "Text");
        }

        private SelectList supplierSelectList(List<Supplier> ss)
        {
            return new SelectList((from s in ss
                                   select new SelectListItem
                                   {
                                       Value = s.Id.ToString(),
                                       Text = s.ToString(),
                                   }).ToList(), "Value", "Text");
        }
    }
}