USE [master]
GO
/****** Object:  Database [risdb]    Script Date: 21-May-17 14:31:27 ******/
CREATE DATABASE [risdb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'risdb', FILENAME = N'C:\Users\jkeleme\risdb.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'risdb_log', FILENAME = N'C:\Users\jkeleme\risdb_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [risdb] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [risdb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [risdb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [risdb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [risdb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [risdb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [risdb] SET ARITHABORT OFF 
GO
ALTER DATABASE [risdb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [risdb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [risdb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [risdb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [risdb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [risdb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [risdb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [risdb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [risdb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [risdb] SET  DISABLE_BROKER 
GO
ALTER DATABASE [risdb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [risdb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [risdb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [risdb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [risdb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [risdb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [risdb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [risdb] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [risdb] SET  MULTI_USER 
GO
ALTER DATABASE [risdb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [risdb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [risdb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [risdb] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [risdb] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [risdb] SET QUERY_STORE = OFF
GO
USE [risdb]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [risdb]
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 21-May-17 14:31:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](80) NOT NULL,
	[LastName] [nvarchar](80) NOT NULL,
	[Email] [nvarchar](254) NOT NULL,
	[PasswordHash] [nvarchar](32) NOT NULL,
	[OfficeId] [int] NULL,
	[RoleId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmployeeRole]    Script Date: 21-May-17 14:31:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmployeeRole](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](80) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExternalOrder]    Script Date: 21-May-17 14:31:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExternalOrder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IssuedOn] [datetime] NOT NULL,
	[SupplierId] [int] NOT NULL,
	[EmployeeId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ExternalOrderMedicine]    Script Date: 21-May-17 14:31:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExternalOrderMedicine](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ExternalOrderId] [int] NOT NULL,
	[MedicineId] [int] NOT NULL,
	[Amount] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LocalSupply]    Script Date: 21-May-17 14:31:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LocalSupply](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Supply] [int] NOT NULL,
	[SafetySupply] [int] NULL,
	[MedicineId] [int] NOT NULL,
	[OfficeId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Medicine]    Script Date: 21-May-17 14:31:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Medicine](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](80) NOT NULL,
	[Description] [nvarchar](200) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MedicineSupplier]    Script Date: 21-May-17 14:31:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicineSupplier](
	[MedicineId] [int] NOT NULL,
	[SupplierId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MedicineId] ASC,
	[SupplierId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Office]    Script Date: 21-May-17 14:31:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Office](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](80) NOT NULL,
	[Address] [nvarchar](200) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Perscription]    Script Date: 21-May-17 14:31:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Perscription](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CardNumber] [nvarchar](80) NOT NULL,
	[IssuedOn] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PerscriptionMedicine]    Script Date: 21-May-17 14:31:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PerscriptionMedicine](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PerscriptionId] [int] NOT NULL,
	[MedicineId] [int] NOT NULL,
	[Amount] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Supplier]    Script Date: 21-May-17 14:31:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Supplier](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](80) NOT NULL,
	[Address] [nvarchar](200) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Employee] ON 

INSERT [dbo].[Employee] ([Id], [FirstName], [LastName], [Email], [PasswordHash], [OfficeId], [RoleId]) VALUES (1, N'Halid', N'Bešlic', N'halid@apoteka.com', N'827ccb0eea8a706c4c34a16891f84e7b', NULL, NULL)
INSERT [dbo].[Employee] ([Id], [FirstName], [LastName], [Email], [PasswordHash], [OfficeId], [RoleId]) VALUES (2, N'Siniša', N'Vuco', N'sinisa@apoteka.com', N'827ccb0eea8a706c4c34a16891f84e7b', NULL, NULL)
INSERT [dbo].[Employee] ([Id], [FirstName], [LastName], [Email], [PasswordHash], [OfficeId], [RoleId]) VALUES (3, N'Zoka', N'Bosanac', N'zoka@apoteka.com', N'827ccb0eea8a706c4c34a16891f84e7b', NULL, NULL)
SET IDENTITY_INSERT [dbo].[Employee] OFF
SET IDENTITY_INSERT [dbo].[ExternalOrder] ON 

INSERT [dbo].[ExternalOrder] ([Id], [IssuedOn], [SupplierId], [EmployeeId]) VALUES (1, CAST(N'2017-05-20T23:14:44.000' AS DateTime), 1, 2)
INSERT [dbo].[ExternalOrder] ([Id], [IssuedOn], [SupplierId], [EmployeeId]) VALUES (2, CAST(N'2017-05-20T23:14:44.000' AS DateTime), 1, 2)
INSERT [dbo].[ExternalOrder] ([Id], [IssuedOn], [SupplierId], [EmployeeId]) VALUES (3, CAST(N'2017-05-20T23:14:44.867' AS DateTime), 1, 3)
INSERT [dbo].[ExternalOrder] ([Id], [IssuedOn], [SupplierId], [EmployeeId]) VALUES (4, CAST(N'2017-06-20T23:14:44.000' AS DateTime), 1, 2)
INSERT [dbo].[ExternalOrder] ([Id], [IssuedOn], [SupplierId], [EmployeeId]) VALUES (5, CAST(N'2017-05-20T23:14:44.867' AS DateTime), 2, 3)
INSERT [dbo].[ExternalOrder] ([Id], [IssuedOn], [SupplierId], [EmployeeId]) VALUES (6, CAST(N'2017-05-20T23:14:44.867' AS DateTime), 3, 3)
INSERT [dbo].[ExternalOrder] ([Id], [IssuedOn], [SupplierId], [EmployeeId]) VALUES (7, CAST(N'2017-05-21T01:09:38.000' AS DateTime), 2, 1)
SET IDENTITY_INSERT [dbo].[ExternalOrder] OFF
SET IDENTITY_INSERT [dbo].[ExternalOrderMedicine] ON 

INSERT [dbo].[ExternalOrderMedicine] ([Id], [ExternalOrderId], [MedicineId], [Amount]) VALUES (4, 2, 2, 4)
INSERT [dbo].[ExternalOrderMedicine] ([Id], [ExternalOrderId], [MedicineId], [Amount]) VALUES (5, 2, 3, 5)
INSERT [dbo].[ExternalOrderMedicine] ([Id], [ExternalOrderId], [MedicineId], [Amount]) VALUES (6, 3, 1, 6)
INSERT [dbo].[ExternalOrderMedicine] ([Id], [ExternalOrderId], [MedicineId], [Amount]) VALUES (8, 5, 3, 8)
INSERT [dbo].[ExternalOrderMedicine] ([Id], [ExternalOrderId], [MedicineId], [Amount]) VALUES (9, 6, 3, 9)
INSERT [dbo].[ExternalOrderMedicine] ([Id], [ExternalOrderId], [MedicineId], [Amount]) VALUES (10, 7, 1, 1)
INSERT [dbo].[ExternalOrderMedicine] ([Id], [ExternalOrderId], [MedicineId], [Amount]) VALUES (11, 7, 2, 2)
INSERT [dbo].[ExternalOrderMedicine] ([Id], [ExternalOrderId], [MedicineId], [Amount]) VALUES (1012, 1, 2, 5)
INSERT [dbo].[ExternalOrderMedicine] ([Id], [ExternalOrderId], [MedicineId], [Amount]) VALUES (1013, 1, 3, 3)
INSERT [dbo].[ExternalOrderMedicine] ([Id], [ExternalOrderId], [MedicineId], [Amount]) VALUES (1014, 1, 1, 10)
INSERT [dbo].[ExternalOrderMedicine] ([Id], [ExternalOrderId], [MedicineId], [Amount]) VALUES (1015, 4, 1, 8)
INSERT [dbo].[ExternalOrderMedicine] ([Id], [ExternalOrderId], [MedicineId], [Amount]) VALUES (1016, 4, 3, 9)
SET IDENTITY_INSERT [dbo].[ExternalOrderMedicine] OFF
SET IDENTITY_INSERT [dbo].[Medicine] ON 

INSERT [dbo].[Medicine] ([Id], [Name], [Description]) VALUES (1, N'Bumortin', N'10 kuglica')
INSERT [dbo].[Medicine] ([Id], [Name], [Description]) VALUES (2, N'Max Flu', N'10 tableta')
INSERT [dbo].[Medicine] ([Id], [Name], [Description]) VALUES (3, N'Flaster', N'100 komada')
SET IDENTITY_INSERT [dbo].[Medicine] OFF
SET IDENTITY_INSERT [dbo].[Supplier] ON 

INSERT [dbo].[Supplier] ([Id], [Name], [Address]) VALUES (1, N'Zinex', N'Z. Pezelja 18')
INSERT [dbo].[Supplier] ([Id], [Name], [Address]) VALUES (2, N'Zoo', N'Maksimirski perivoj bb')
INSERT [dbo].[Supplier] ([Id], [Name], [Address]) VALUES (3, N'JT International', N'Radnicka cesta 34')
SET IDENTITY_INSERT [dbo].[Supplier] OFF
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__Employee__737584F65B8D4BA2]    Script Date: 21-May-17 14:31:27 ******/
ALTER TABLE [dbo].[EmployeeRole] ADD UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD FOREIGN KEY([OfficeId])
REFERENCES [dbo].[Office] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD FOREIGN KEY([RoleId])
REFERENCES [dbo].[EmployeeRole] ([Id])
GO
ALTER TABLE [dbo].[ExternalOrder]  WITH CHECK ADD FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([Id])
ON DELETE SET NULL
GO
ALTER TABLE [dbo].[ExternalOrder]  WITH CHECK ADD FOREIGN KEY([SupplierId])
REFERENCES [dbo].[Supplier] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ExternalOrderMedicine]  WITH CHECK ADD FOREIGN KEY([ExternalOrderId])
REFERENCES [dbo].[ExternalOrder] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ExternalOrderMedicine]  WITH CHECK ADD FOREIGN KEY([MedicineId])
REFERENCES [dbo].[Medicine] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[LocalSupply]  WITH CHECK ADD FOREIGN KEY([MedicineId])
REFERENCES [dbo].[Medicine] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[LocalSupply]  WITH CHECK ADD FOREIGN KEY([OfficeId])
REFERENCES [dbo].[Office] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MedicineSupplier]  WITH CHECK ADD FOREIGN KEY([MedicineId])
REFERENCES [dbo].[Medicine] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[MedicineSupplier]  WITH CHECK ADD FOREIGN KEY([SupplierId])
REFERENCES [dbo].[Supplier] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PerscriptionMedicine]  WITH CHECK ADD FOREIGN KEY([MedicineId])
REFERENCES [dbo].[Medicine] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[PerscriptionMedicine]  WITH CHECK ADD FOREIGN KEY([PerscriptionId])
REFERENCES [dbo].[Perscription] ([Id])
ON DELETE CASCADE
GO
USE [master]
GO
ALTER DATABASE [risdb] SET  READ_WRITE 
GO
