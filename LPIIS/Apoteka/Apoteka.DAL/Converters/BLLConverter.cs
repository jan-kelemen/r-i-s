﻿using Apoteka.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apoteka.DAL.Converters
{
    public static class BLLConverter
    {
        public static Employee ConvertEmployee(Model.Employee e)
        {
            if(e == null) { return null; }

            var office = ConvertOffice(e.Office);
            var role = ConvertEmployeeRole(e.EmployeeRole);
            return new Employee(e.Id, e.FirstName, e.LastName, e.Email, e.PasswordHash, office, role);
        }

        public static EmployeeRole ConvertEmployeeRole(Model.EmployeeRole e)
        {
            if (e == null) { return null; }

            return new EmployeeRole(e.Id, e.Name);
        }

        public static ExternalOrder ConvertExternalOrder(Model.ExternalOrder e)
        {
            if (e == null) { return null; }

            var supplier = ConvertSupplier(e.Supplier);
            var employee = ConvertEmployee(e.Employee);
            return new ExternalOrder(e.Id, e.IssuedOn, supplier, employee);
        }

        public static ExternalOrderMedicine ConvertExternalOrderMedicine(Model.ExternalOrderMedicine e)
        {
            if (e == null) { return null; }

            var medicine = ConvertMedicine(e.Medicine);
            return new ExternalOrderMedicine(e.Id, medicine, e.Amount);
        }

        public static LocalSupply ConvertLocalSupply(Model.LocalSupply e)
        {
            if (e == null) { return null; }

            var medicine = ConvertMedicine(e.Medicine);
            return new LocalSupply(e.Id, medicine, e.Supply, e.SafetySupply);
        }

        public static Medicine ConvertMedicine(Model.Medicine e)
        {
            if (e == null) { return null; }

            return new Medicine(e.Id, e.Name, e.Description);
        }

        public static Office ConvertOffice(Model.Office e)
        {
            if (e == null) { return null; }

            return new Office(e.Id, e.Name, e.Address);
        }

        public static Perscription ConvertPerscription(Model.Perscription e)
        {
            if (e == null) { return null; }

            return new Perscription(e.Id, e.CardNumber, e.IssuedOn);
        }

        public static PerscriptionMedicine ConvertPerscriptionMedicine(Model.PerscriptionMedicine e)
        {
            if (e == null) { return null; }

            var medicine = ConvertMedicine(e.Medicine);
            return new PerscriptionMedicine(e.Id, medicine, e.Amount);
        }

        public static Supplier ConvertSupplier(Model.Supplier e)
        {
            if (e == null) { return null; }

            return new Supplier(e.Id, e.Name, e.Address);
        }
    }
}
