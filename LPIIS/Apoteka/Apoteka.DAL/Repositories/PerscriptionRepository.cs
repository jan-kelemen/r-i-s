﻿using Apoteka.BLL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Apoteka.BLL.Models;
using Apoteka.DAL.Converters;

namespace Apoteka.DAL.Repositories
{
    public class PerscriptionRepository : BaseRepository<Model.Perscription, Perscription>, IPerscriptionRepository
    {
        public PerscriptionRepository(Model.RISEntities db) : base(db)
        {
        }

        public List<PerscriptionMedicine> GetMedicines(int id)
        {
            var medicines = db.PerscriptionMedicines.Where(l => l.PerscriptionId == id);

            var rv = new List<PerscriptionMedicine>();
            foreach(var m in medicines)
            {
                rv.Add(BLLConverter.ConvertPerscriptionMedicine(m));
            }
            return rv;
        }

        public List<PerscriptionMedicine> GetMedicines(Perscription eo)
        {
            return GetMedicines(eo.Id);
        }

        public List<PerscriptionMedicine> UpdateMedicines(int id, List<PerscriptionMedicine> eom)
        {
            throw new NotImplementedException();
        }

        public List<PerscriptionMedicine> UpdateMedicines(Perscription eo, List<PerscriptionMedicine> eom)
        {
            return UpdateMedicines(eo.Id, eom);
        }

        protected override Perscription convertToBLL(Model.Perscription e)
        {
            return BLLConverter.ConvertPerscription(e);
        }

        protected override Model.Perscription convertToDAL(Perscription e)
        {
            return DALConverter.ConvertPerscription(e);
        }

        protected override void updateProperties(Model.Perscription dal, Perscription bll)
        {
            dal.CardNumber = bll.CardNumber;
            dal.IssuedOn = bll.IssuedOn;
        }
    }
}
