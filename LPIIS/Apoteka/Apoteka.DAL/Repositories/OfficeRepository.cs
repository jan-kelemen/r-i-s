﻿using Apoteka.BLL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Apoteka.BLL.Models;
using Apoteka.DAL.Converters;

namespace Apoteka.DAL.Repositories
{
    public class OfficeRepository : BaseRepository<Model.Office, Office>, IOfficeRepository
    {
        public OfficeRepository(Model.RISEntities db) : base(db)
        {
        }

        public List<LocalSupply> GetSupply(int id)
        {
            var supply = db.LocalSupplies.Where(l => l.OfficeId == id);

            var rv = new List<LocalSupply>();
            foreach (var s in supply)
            {
                rv.Add(BLLConverter.ConvertLocalSupply(s));
            }

            return rv;
        }

        public List<LocalSupply> GetSupply(Office of)
        {
            return GetSupply(of.Id);
        }

        public List<LocalSupply> UpdateSupply(int id, List<LocalSupply> ls)
        {
            throw new NotImplementedException();
        }

        public List<LocalSupply> UpdateSupply(Office of, List<LocalSupply> ls)
        {
            return UpdateSupply(of.Id, ls);
        }

        protected override Office convertToBLL(Model.Office e)
        {
            return BLLConverter.ConvertOffice(e);
        }

        protected override Model.Office convertToDAL(Office e)
        {
            return DALConverter.ConvertOffice(e);
        }

        protected override void updateProperties(Model.Office dal, Office bll)
        {
            dal.Name = bll.Name;
            dal.Address = bll.Address;
        }
    }
}
