USE [master]
GO
/****** Object:  Database [risdb]    Script Date: 15-Apr-17 22:46:09 ******/
CREATE DATABASE [risdb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'risdb', FILENAME = N'C:\Users\jkele\risdb.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'risdb_log', FILENAME = N'C:\Users\jkele\risdb_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [risdb] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [risdb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [risdb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [risdb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [risdb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [risdb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [risdb] SET ARITHABORT OFF 
GO
ALTER DATABASE [risdb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [risdb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [risdb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [risdb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [risdb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [risdb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [risdb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [risdb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [risdb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [risdb] SET  DISABLE_BROKER 
GO
ALTER DATABASE [risdb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [risdb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [risdb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [risdb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [risdb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [risdb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [risdb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [risdb] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [risdb] SET  MULTI_USER 
GO
ALTER DATABASE [risdb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [risdb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [risdb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [risdb] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [risdb] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [risdb] SET QUERY_STORE = OFF
GO
USE [risdb]
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [risdb]
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 15-Apr-17 22:46:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](128) NOT NULL,
	[LastName] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](254) NOT NULL,
	[PasswordHash] [nchar](256) NOT NULL,
	[OfficeId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ExternalOrder]    Script Date: 15-Apr-17 22:46:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExternalOrder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IssuedOn] [datetime] NOT NULL,
	[SupplierId] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ExternalOrderMedicine]    Script Date: 15-Apr-17 22:46:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ExternalOrderMedicine](
	[ExternalOrderId] [int] NOT NULL,
	[MedicineId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ExternalOrderId] ASC,
	[MedicineId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LocalSupply]    Script Date: 15-Apr-17 22:46:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LocalSupply](
	[OfficeId] [int] NOT NULL,
	[MedicineId] [int] NOT NULL,
	[Supply] [int] NOT NULL,
	[SafetySupply] [int] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Medicine]    Script Date: 15-Apr-17 22:46:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Medicine](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Dose] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MedicineSupplier]    Script Date: 15-Apr-17 22:46:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicineSupplier](
	[MedicineId] [int] NOT NULL,
	[SupplierId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[MedicineId] ASC,
	[SupplierId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Office]    Script Date: 15-Apr-17 22:46:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Office](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Address] [nvarchar](128) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Perscription]    Script Date: 15-Apr-17 22:46:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Perscription](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CardNumber] [nvarchar](128) NOT NULL,
	[IssuedOn] [datetime] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PerscriptionMedicine]    Script Date: 15-Apr-17 22:46:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PerscriptionMedicine](
	[PerscriptionId] [int] NOT NULL,
	[MedicineId] [int] NOT NULL,
	[Amount] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[PerscriptionId] ASC,
	[MedicineId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Supplier]    Script Date: 15-Apr-17 22:46:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Supplier](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Address] [nvarchar](128) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 15-Apr-17 22:46:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Employee] ON 

INSERT [dbo].[Employee] ([Id], [FirstName], [LastName], [Email], [PasswordHash], [OfficeId], [RoleId]) VALUES (1, N'Zeljko', N'Bebek', N'zbebek@dugme.hr', N'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3                                                                                                                                                                                                ', 1, 1)
INSERT [dbo].[Employee] ([Id], [FirstName], [LastName], [Email], [PasswordHash], [OfficeId], [RoleId]) VALUES (2, N'Radmila', N'M', N'rm@dugme.hr', N'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3                                                                                                                                                                                                ', 1, 2)
SET IDENTITY_INSERT [dbo].[Employee] OFF
INSERT [dbo].[LocalSupply] ([OfficeId], [MedicineId], [Supply], [SafetySupply]) VALUES (1, 1, 100, 0)
INSERT [dbo].[LocalSupply] ([OfficeId], [MedicineId], [Supply], [SafetySupply]) VALUES (1, 2, 50, 30)
INSERT [dbo].[LocalSupply] ([OfficeId], [MedicineId], [Supply], [SafetySupply]) VALUES (1, 3, 300, 100)
SET IDENTITY_INSERT [dbo].[Medicine] ON 

INSERT [dbo].[Medicine] ([Id], [Name], [Dose]) VALUES (1, N'Aspirin', 5)
INSERT [dbo].[Medicine] ([Id], [Name], [Dose]) VALUES (2, N'Lupocet', 5)
INSERT [dbo].[Medicine] ([Id], [Name], [Dose]) VALUES (3, N'Melem', 5)
INSERT [dbo].[Medicine] ([Id], [Name], [Dose]) VALUES (4, N'Neofen', 5)
SET IDENTITY_INSERT [dbo].[Medicine] OFF
INSERT [dbo].[MedicineSupplier] ([MedicineId], [SupplierId]) VALUES (1, 1)
INSERT [dbo].[MedicineSupplier] ([MedicineId], [SupplierId]) VALUES (1, 2)
INSERT [dbo].[MedicineSupplier] ([MedicineId], [SupplierId]) VALUES (2, 1)
INSERT [dbo].[MedicineSupplier] ([MedicineId], [SupplierId]) VALUES (2, 2)
INSERT [dbo].[MedicineSupplier] ([MedicineId], [SupplierId]) VALUES (3, 1)
SET IDENTITY_INSERT [dbo].[Office] ON 

INSERT [dbo].[Office] ([Id], [Name], [Address]) VALUES (1, N'Prodavaonica 1', N'Au 2')
INSERT [dbo].[Office] ([Id], [Name], [Address]) VALUES (2, N'Prodavaonica 2', N'Ri 3')
SET IDENTITY_INSERT [dbo].[Office] OFF
SET IDENTITY_INSERT [dbo].[Perscription] ON 

INSERT [dbo].[Perscription] ([Id], [CardNumber], [IssuedOn]) VALUES (1, N'123', CAST(N'2017-04-15T22:20:32.667' AS DateTime))
INSERT [dbo].[Perscription] ([Id], [CardNumber], [IssuedOn]) VALUES (2, N'456', CAST(N'2017-04-15T22:20:32.667' AS DateTime))
SET IDENTITY_INSERT [dbo].[Perscription] OFF
INSERT [dbo].[PerscriptionMedicine] ([PerscriptionId], [MedicineId], [Amount]) VALUES (1, 1, 10)
INSERT [dbo].[PerscriptionMedicine] ([PerscriptionId], [MedicineId], [Amount]) VALUES (1, 2, 5)
INSERT [dbo].[PerscriptionMedicine] ([PerscriptionId], [MedicineId], [Amount]) VALUES (1, 3, 3)
SET IDENTITY_INSERT [dbo].[Supplier] ON 

INSERT [dbo].[Supplier] ([Id], [Name], [Address]) VALUES (1, N'Pliva', N'Zg 1')
INSERT [dbo].[Supplier] ([Id], [Name], [Address]) VALUES (2, N'Medika', N'Zg 2')
SET IDENTITY_INSERT [dbo].[Supplier] OFF
SET IDENTITY_INSERT [dbo].[UserRole] ON 

INSERT [dbo].[UserRole] ([Id], [Name]) VALUES (1, N'Cashier')
INSERT [dbo].[UserRole] ([Id], [Name]) VALUES (2, N'PurchaseManager')
SET IDENTITY_INSERT [dbo].[UserRole] OFF
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD FOREIGN KEY([OfficeId])
REFERENCES [dbo].[Office] ([Id])
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD FOREIGN KEY([RoleId])
REFERENCES [dbo].[UserRole] ([Id])
GO
ALTER TABLE [dbo].[ExternalOrder]  WITH CHECK ADD FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employee] ([Id])
GO
ALTER TABLE [dbo].[ExternalOrder]  WITH CHECK ADD FOREIGN KEY([SupplierId])
REFERENCES [dbo].[Supplier] ([Id])
GO
ALTER TABLE [dbo].[ExternalOrderMedicine]  WITH CHECK ADD FOREIGN KEY([ExternalOrderId])
REFERENCES [dbo].[ExternalOrder] ([Id])
GO
ALTER TABLE [dbo].[ExternalOrderMedicine]  WITH CHECK ADD FOREIGN KEY([MedicineId])
REFERENCES [dbo].[Medicine] ([Id])
GO
ALTER TABLE [dbo].[LocalSupply]  WITH CHECK ADD FOREIGN KEY([MedicineId])
REFERENCES [dbo].[Medicine] ([Id])
GO
ALTER TABLE [dbo].[LocalSupply]  WITH CHECK ADD FOREIGN KEY([OfficeId])
REFERENCES [dbo].[Office] ([Id])
GO
ALTER TABLE [dbo].[MedicineSupplier]  WITH CHECK ADD FOREIGN KEY([MedicineId])
REFERENCES [dbo].[Medicine] ([Id])
GO
ALTER TABLE [dbo].[MedicineSupplier]  WITH CHECK ADD FOREIGN KEY([SupplierId])
REFERENCES [dbo].[Supplier] ([Id])
GO
ALTER TABLE [dbo].[PerscriptionMedicine]  WITH CHECK ADD FOREIGN KEY([MedicineId])
REFERENCES [dbo].[Medicine] ([Id])
GO
ALTER TABLE [dbo].[PerscriptionMedicine]  WITH CHECK ADD FOREIGN KEY([PerscriptionId])
REFERENCES [dbo].[Perscription] ([Id])
GO
USE [master]
GO
ALTER DATABASE [risdb] SET  READ_WRITE 
GO
